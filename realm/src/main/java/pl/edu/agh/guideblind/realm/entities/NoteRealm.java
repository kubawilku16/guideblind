package pl.edu.agh.guideblind.realm.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class NoteRealm extends RealmObject {
    @PrimaryKey
    private Long date;
    private String content;
}