package pl.edu.agh.guideblind.realm.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContactRealm extends RealmObject {
    @PrimaryKey
    private String id;
    private String name;
    private RealmList<PhoneRealm> phones;
}