package pl.edu.agh.guideblind.realm.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ConversationRealm extends RealmObject {
    @PrimaryKey
    private String contactId;
    private RealmList<MessageRealm> messages;

    public void addMessage(MessageRealm message) {
        this.messages.add(message);
    }

    public void deleteMessage(MessageRealm message) {
        this.messages.remove(message);
    }
}