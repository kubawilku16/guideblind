package pl.edu.agh.guideblind.realm.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PhoneRealm extends RealmObject {
    @PrimaryKey
    private String number;
}