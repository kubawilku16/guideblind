package pl.edu.agh.guideblind.realm.provider;

import io.realm.annotations.RealmModule;

@RealmModule(library = true, allClasses = true)
class GuideBlindRealmModule {
}