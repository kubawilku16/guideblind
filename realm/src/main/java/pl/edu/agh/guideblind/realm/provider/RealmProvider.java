package pl.edu.agh.guideblind.realm.provider;

import android.content.Context;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class RealmProvider {
    private static Context applicationContext;
    private static RealmConfiguration configuration;

    protected RealmProvider() {
    }

    public static void init(Context context) {
        Realm.init(context);
        applicationContext = context;
        new RealmProvider();
    }

    private static RealmConfiguration getConfiguration() {
        if (configuration == null) {
            configuration = new RealmConfiguration.Builder()
                    .name("guideBlind.realm")
                    .modules(new GuideBlindRealmModule())
                    .deleteRealmIfMigrationNeeded()
                    .build();
        }
        return configuration;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static <T extends RealmObject> T findById(Class<? extends RealmObject> clazz, String keyName, String id) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }

            RealmObject result = realm.where(clazz).equalTo(keyName, id).findFirst();
            if (result != null) {
                return (T) realm.copyFromRealm(result);
            } else {
                return null;
            }
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    public static <T extends RealmObject> T insertOrUpdate(T object) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }
            realm.beginTransaction();
            realm.insertOrUpdate(object);
            realm.commitTransaction();
            return object;
        } catch (Exception e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            throw e;
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    public static <T extends RealmObject> List<T> insertOrUpdate(List<T> objects) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }
            realm.beginTransaction();
            realm.insertOrUpdate(objects);
            realm.commitTransaction();
            return objects;
        } catch (Exception e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            throw e;
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends RealmObject> List<T> findAll(Class<? extends RealmObject> T) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }
            RealmResults<? extends RealmObject> results = realm.where(T).findAll();
            if (results != null) {
                return (List<T>) realm.copyFromRealm(results);
            } else {
                return new ArrayList<>();
            }
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    public static void delete(Class<? extends RealmObject> clazz, String keyName, String id) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }
            realm.beginTransaction();
            RealmObject result = realm.where(clazz).equalTo(keyName, id).findFirst();
            if (result != null) {
                result.deleteFromRealm();
            }
            realm.commitTransaction();
        } catch (Exception e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            throw e;
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    public static void delete(Class<? extends RealmObject> clazz, String keyName, Long id) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }
            realm.beginTransaction();
            RealmObject result = realm.where(clazz).equalTo(keyName, id).findFirst();
            if (result != null) {
                result.deleteFromRealm();
            }
            realm.commitTransaction();
        } catch (Exception e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            throw e;
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    public static void deleteAll(Class<? extends RealmObject> T) {
        Realm realm = Realm.getInstance(getConfiguration());
        try {
            if (realm.isClosed()) {
                Realm.init(applicationContext);
            }
            realm.beginTransaction();
            realm.delete(T);
            realm.commitTransaction();
        } catch (Exception e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            throw e;
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    public static void clearRealm() {
        Realm.deleteRealm(getConfiguration());
    }
}