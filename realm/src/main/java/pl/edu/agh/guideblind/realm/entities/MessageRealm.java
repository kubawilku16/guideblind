package pl.edu.agh.guideblind.realm.entities;

import io.realm.RealmObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MessageRealm extends RealmObject {
    private String content;
    private long date;
    private String type;
}