package pl.edu.agh.guideblind.messages.incoming;

import pl.edu.agh.guideblind.base.BasePresenter;

class IncomingMessagesPresenter extends BasePresenter<IncomingMessagesActivity> {
    void onInit() {
        view.addPages();
        view.initPager();
    }
}