package pl.edu.agh.guideblind.messages.page.viewer.conversations.page;

import android.util.Log;

import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetContactByIdInteractor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;

class ConversationPresenter extends BasePresenter<ConversationFragment> {
    void onInit() {
        getContactName();
    }

    private void getContactName() {
        new GetContactByIdInteractor(view.getConversation()).execute().subscribe(this::onNext, this::onError);
    }

    private void onNext(ContactRealm contactRealm) {
        if (contactRealm != null) {
            view.setNameViewText(contactRealm.getName());
            speak(contactRealm.getName());
        } else {
            view.setNameViewText(view.getConversation());
        }
    }

    private void onError(Throwable throwable) {
        Log.e(getClass().getName(), throwable.getMessage());
        view.callOnBack();
    }

    void onButtonClick() {
        if (view != null) {
            view.startTimer();
        }
    }
}