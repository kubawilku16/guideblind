package pl.edu.agh.guideblind.contacts.page.number;

import android.util.Log;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetContactByNumberInteractor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;


public class AddNumberPresenter extends BasePresenter<AddNumberFragment> {

    void onButtonClick() {
        if(view != null) {
            view.startTimer();
        }
    }

    void onLongButtonClick() {
        if(view != null) {
            view.hideTextView();
            view.startSpeechRecognizerIntent();
        }
    }

    void findContactByNumber(String number) {
        new GetContactByNumberInteractor(number).execute().subscribe(this::onNext, this::onError);
    }

    private void onNext(ContactRealm contactRealm) {
        if (contactRealm == null) {
            view.setContactNumberContactActivity();
        }else {
            speak(view.getResources().getString(R.string.contact_exist));
        }
    }

    private void onError(Throwable throwable) {
        speak(view.getResources().getString(R.string.unrecognized_error));
        Log.e(getClass().getName(), throwable.getMessage());
    }
}
