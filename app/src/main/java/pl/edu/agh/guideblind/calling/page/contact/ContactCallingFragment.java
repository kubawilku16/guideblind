package pl.edu.agh.guideblind.calling.page.contact;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Arrays;
import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.find_by_name_page)
public class ContactCallingFragment extends BaseFragment<ContactCallingPresenter> {

    @ViewById(R.id.find_by_name_page_text)
    protected TextView textView;

    @Override
    public ContactCallingPresenter createPresenter() {
        return new ContactCallingPresenter();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.tell_contact));
    }

    @Click(R.id.find_by_name_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @LongClick(R.id.find_by_name_layout)
    public boolean onLongClick() {
        presenter.onLongButtonClick();
        return true;
    }

    /**
     * Showing google speech input dialog
     */
    void startSpeechRecognizerIntent() {
        SpeechRecognizer.startIntent(this);
    }

    /**
     * Receiving speech input
     */
    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            List<String> words = Arrays.asList(data.getStringArrayListExtra(EXTRA_RESULTS).get(0).split(" "));
            String contactName = words.size() < 2 ?
                    words.get(0) : words.get(0) + " " + words.get(1);
            contactName = WordUtils.capitalize(contactName);
            textView.setText(contactName);
            textView.setVisibility(View.VISIBLE);
            presenter.findContact(contactName);
        }
    }

    void call(ContactRealm contact) {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getActivity().startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + contact.getPhones().get(0).getNumber())));
        }
    }

    void hideTextView() {
        textView.setVisibility(View.INVISIBLE);
    }
}
