package pl.edu.agh.guideblind.interactor;

import lombok.AllArgsConstructor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import rx.Observable;

@AllArgsConstructor
public class GetContactByIdInteractor {
    private String id;

    public Observable<ContactRealm> execute() {
        return Observable.defer(this::findContact);
    }

    private Observable<ContactRealm> findContact() {
        return Observable.just(RealmProvider.findById(ContactRealm.class, "id", id));
    }
}