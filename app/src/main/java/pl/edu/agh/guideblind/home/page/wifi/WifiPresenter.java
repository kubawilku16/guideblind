package pl.edu.agh.guideblind.home.page.wifi;

import pl.edu.agh.guideblind.base.BasePresenter;

class WifiPresenter extends BasePresenter<WifiFragment> {

    void onInit() {
        view.initWifiManager();
        view.setImage();
        view.initWifiStateReceiver();
    }

    void onClick() {
        if (view != null) {
            view.speak();
        }
    }

    void onLongClick() {
        if (view != null) {
            if(view.isWifiEnabled()){
                view.startSpotsActivity();
            } else {
                view.changeWifiStatus();
            }
        }
    }
}