package pl.edu.agh.guideblind.pictureRecognition.pictureRecognitonHome;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.pictureRecognition.PictureRecognitionActivity_;

@EFragment(R.layout.page_with_view)
public class PictureRecognitionFragment extends BaseFragment<PictureRecognitionFragmentPresenter> {

    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Override
    protected PictureRecognitionFragmentPresenter createPresenter() {
        return new PictureRecognitionFragmentPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_image)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.picture_recognition));
    }

    public void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.picture_recognition));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
            wentToDownIntent = true;
            PictureRecognitionActivity_.intent(getActivity()).start();
        }
    }
}
