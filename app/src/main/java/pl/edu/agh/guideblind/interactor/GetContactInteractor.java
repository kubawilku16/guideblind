package pl.edu.agh.guideblind.interactor;


import com.annimon.stream.Stream;

import java.util.List;

import lombok.AllArgsConstructor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import pl.edu.agh.guideblind.utils.RxUtils;
import rx.Observable;

@AllArgsConstructor
public class GetContactInteractor {
    private String contactName;

    public Observable<ContactRealm> execute() {
        return searchForContact(RealmProvider.findAll(ContactRealm.class))
                .compose(RxUtils.applySchedulers());
    }

    private Observable<ContactRealm> searchForContact(List<ContactRealm> contacts) {
        return contacts != null && !contacts.isEmpty() ?
                Observable.just(Stream.of(contacts)
                        .filter(contact -> contact.getName().toLowerCase().replaceAll(" ", "").equals(contactName.toLowerCase().replaceAll(" ", "")))
                        .findFirst()
                        .orElse(null)) : null;
    }
}