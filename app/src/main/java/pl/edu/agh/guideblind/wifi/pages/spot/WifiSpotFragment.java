package pl.edu.agh.guideblind.wifi.pages.spot;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import lombok.Getter;
import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.page_with_view)
public class WifiSpotFragment extends BaseFragment<WifiSpotPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView imageView;

    @Getter
    @FragmentArg
    String SSID;
    @Getter
    @FragmentArg
    String capabilities;
    @Getter
    @FragmentArg
    boolean isProtected;

    private WifiManager wifiManager;

    @Override
    protected WifiSpotPresenter createPresenter() {
        return new WifiSpotPresenter();
    }

    @Override
    public void speak() {
        presenter.speak(SSID + ". " + getResources().getString(isProtected ? R.string.wifi_locked : R.string.wifi_connect));
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        presenter.onClick();
    }

    @LongClick(R.id.page_with_view_layout)
    void onLongClick() {
        presenter.onLongClick();
    }

    void setImage() {
        imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), isProtected ? R.drawable.wifi_spot_locked : R.drawable.wifi_spot));
    }

    void initWifiManager() {
        wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    void addWifiConfiguration(WifiConfiguration wifiConfiguration) {
        wifiManager.addNetwork(wifiConfiguration);
    }

    void startSpeechRecognizer() {
        SpeechRecognizer.startIntent(this);
    }

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            presenter.createWifiConfiguration(data.getStringArrayListExtra(EXTRA_RESULTS).get(0).replaceAll(" ", ""));
        }
    }
}