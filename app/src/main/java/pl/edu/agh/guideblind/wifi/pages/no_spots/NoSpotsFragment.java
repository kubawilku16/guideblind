package pl.edu.agh.guideblind.wifi.pages.no_spots;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;

@EFragment(R.layout.page_with_view)
public class NoSpotsFragment extends BaseFragment<NoSpotsPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView imageView;

    @Override
    protected NoSpotsPresenter createPresenter() {
        return new NoSpotsPresenter();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.no_spots));
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        presenter.onClick();
    }

    void setImage() {
        imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.no_wifi_spots));
    }
}