package pl.edu.agh.guideblind.utils;

import android.telephony.SmsManager;
import android.util.Log;

import com.annimon.stream.Stream;

public class SmsSendUtils {
    public static boolean sendSms(String content, String[] receivers) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            Stream.of(receivers).forEach(receiver -> smsManager.sendTextMessage(receiver, null, content, null, null));
            return true;
        } catch (Exception e) {
            Log.e("SEND_SMS", e.getMessage());
            return false;
        }
    }
}