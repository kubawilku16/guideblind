package pl.edu.agh.guideblind.contacts;

import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.contacts.contact.ContactAdder;
import pl.edu.agh.guideblind.contacts.page.name.AddContactFinderFragment_;
import pl.edu.agh.guideblind.contacts.page.number.AddNumberFragment_;
import pl.edu.agh.guideblind.contacts.page.save.SaveContactFragment_;
import pl.edu.agh.guideblind.utils.AddContactUtils;

@EActivity(R.layout.pager_layout)
public class ContactActivity extends BaseActivity<ContactPresenter> implements ContactAdder{
    @ViewById(R.id.pager)
    ViewPager mPager;

    private String contactNumber;
    private String contactName;

    @Override
    protected ContactPresenter createPresenter() {
        return new ContactPresenter();
    }

    @AfterViews
    void onInit( ){presenter.onInit();}

    void initPager() {
        pages.add(new AddContactFinderFragment_());
        pages.add(new AddNumberFragment_());
        pages.add(new SaveContactFragment_());
    }

    void initAdapter(){
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void saveContact() {
        if(contactNumber != null && contactNumber.length() > 0 && !TextUtils.isEmpty(contactName)){
            if(AddContactUtils.addContact(this, contactName, contactNumber)){
                presenter.speak(getResources().getString(R.string.contact_added));
            }else{
                presenter.speak(getResources().getString(R.string.save_contact_error));
            }
        }
    }

    @Override
    public void saveContactName(String contactNames) {
        this.contactName = contactNames;
    }

    @Override
    public void saveContactNumber(String contactNumbers) {
        this.contactNumber = contactNumbers;
    }

    @Override
    public String getContactName() {
        if(contactName == null || contactName.length() == 0){
            return null;
        }
        return  contactName;
    }

    @Override
    public String getContactNumber() {
        if (contactNumber == null || contactNumber.length() == 0) {
            return null;
        }
        return contactNumber;
    }
}
