package pl.edu.agh.guideblind.notes.page.new_note_fragment;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFirstFragment;
import pl.edu.agh.guideblind.notes.NotesViewerActivity_;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.page_with_view)
public class NewNoteFragment extends BaseFirstFragment<NewNotePresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Override
    protected NewNotePresenter createPresenter() {
        return new NewNotePresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        presenter.onClick();
    }

    @LongClick(R.id.page_with_view_layout)
    public boolean onLongClick() {
        presenter.onLongButtonLick();
        return true;
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.new_note_hold));
    }

    void startSpeechRecognizerIntent() {
        SpeechRecognizer.startIntent(this);
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.note_add));
    }

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            presenter.saveNote(data.getStringArrayListExtra(EXTRA_RESULTS).get(0));
        }
    }

    void startActivityAgain() {
        NotesViewerActivity_.intent(getActivity()).start();
        getActivity().finish();
    }
}