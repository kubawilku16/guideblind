package pl.edu.agh.guideblind.services.retrofit;

import pl.edu.agh.guideblind.pojo.YandexResponsePojo.YandexResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface YandexTranslationService {
    @POST("api/v1.5/tr.json/translate")
    @FormUrlEncoded
    Observable<YandexResponse> translate(@Query("key")String key, @Query("lang")String lang, @Field("text") String text);
}
