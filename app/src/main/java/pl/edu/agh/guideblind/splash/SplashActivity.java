package pl.edu.agh.guideblind.splash;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.annimon.stream.Stream;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.home.HomeActivity_;

import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_SMS;

@EActivity(R.layout.splash)
public class SplashActivity extends BaseActivity<SplashPresenter> {
    private static final int PERMISSION_ALL = 0;

    @Override
    protected SplashPresenter createPresenter() {
        return new SplashPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        presenter.checkPermissions(grantResults);
    }

    void askForPermissions() {
        if (!UtilPermissions.hasPermissions(getApplicationContext())) {
            presenter.speak(getResources().getString(R.string.ask_for_all_permissions));
            ActivityCompat.requestPermissions(this, UtilPermissions.PERMISSIONS, PERMISSION_ALL);
        } else {
            startHomeActivity();
        }
    }

    void startHomeActivity() {
        HomeActivity_.intent(this).start();
    }

    void finishApp() {
        while(presenter.isSpeaking()){
        }
        finish();
    }
}
