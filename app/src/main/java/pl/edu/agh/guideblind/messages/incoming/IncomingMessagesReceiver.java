package pl.edu.agh.guideblind.messages.incoming;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.guideblind.realm.entities.ConversationRealm;
import pl.edu.agh.guideblind.realm.entities.MessageRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class IncomingMessagesReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            List<String> messages = new LinkedList<>();
            List<String> senders = new LinkedList<>();
            List<Long> dates = new LinkedList<>();
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                String body = smsMessage.getMessageBody();
                String sender =smsMessage.getOriginatingAddress();
                Long date = System.currentTimeMillis();
                ConversationRealm conversation= RealmProvider.findById(ConversationRealm.class, "contactId", sender);
                if(conversation != null) {
                    conversation.addMessage(new MessageRealm(body, date, "1"));
                    RealmProvider.insertOrUpdate(conversation);
                }
                messages.add(body);
                senders.add(sender);
                dates.add(date);
            }
            IncomingMessagesActivity_.intent(context)
                    .flags(FLAG_ACTIVITY_NEW_TASK)
                    .messages(messages.toArray(new String[messages.size()]))
                    .senders(senders.toArray(new String[senders.size()]))
                    .dates(dates.toArray(new Long[dates.size()]))
                    .start();
        }
    }
}