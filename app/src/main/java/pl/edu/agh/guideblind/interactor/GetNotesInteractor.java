package pl.edu.agh.guideblind.interactor;

import java.util.Collections;
import java.util.List;

import pl.edu.agh.guideblind.realm.entities.NoteRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import rx.Observable;

public class GetNotesInteractor {
    public Observable<List<NoteRealm>> execute() {
        return Observable.just(getData());
    }

    private List<NoteRealm> getData() {
        List<NoteRealm> result = getNotes();
        Collections.sort(result, this::sort);
        return result;
    }

    private int sort(NoteRealm note1, NoteRealm note2) {
        if (note1.getDate() > note2.getDate()) {
            return 1;
        } else if (note1.getDate() < note2.getDate()) {
            return -1;
        } else {
            return 0;
        }
    }

    private List<NoteRealm> getNotes() {
        return RealmProvider.findAll(NoteRealm.class);
    }
}