package pl.edu.agh.guideblind.utils;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Timer;
import java.util.TimerTask;
import pl.edu.agh.guideblind.calling.outgoingCall.OutgoingCallActivity_;

public class OutgoingCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                OutgoingCallActivity_.intent(context).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
            }
        }, 1000);
    }
}
