package pl.edu.agh.guideblind.home.page.contact;


import pl.edu.agh.guideblind.base.BasePresenter;

public class ContactPresenter extends BasePresenter<ContactFragment> {

    void onInit() {view.setImage();}

    void onButtonClick(){
        if(view != null){
            view.startTimer();
        }
    }
}
