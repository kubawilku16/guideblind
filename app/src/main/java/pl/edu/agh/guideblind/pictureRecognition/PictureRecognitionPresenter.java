package pl.edu.agh.guideblind.pictureRecognition;

import pl.edu.agh.guideblind.base.BasePresenter;


public class PictureRecognitionPresenter extends BasePresenter<PictureRecognitionActivity> {
    void onInit() {
        view.initPages();
        view.initAdapter();
    }
}
