package pl.edu.agh.guideblind.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseActivity<P extends BasePresenter> extends FragmentActivity implements BaseView {
    protected PagerAdapter mPagerAdapter;
    protected List<Fragment> pages = new ArrayList<>();
    protected P presenter;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        presenter.createView(this);
        presenter.initSpeaker();
    }

    protected abstract P createPresenter();
}