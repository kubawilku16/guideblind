package pl.edu.agh.guideblind.messages;

import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.messages.page.contact.ContactFinderFragment_;
import pl.edu.agh.guideblind.messages.page.creating.CreatingSmsFragment_;
import pl.edu.agh.guideblind.messages.page.number.NumberFinderFragment_;
import pl.edu.agh.guideblind.messages.page.viewer.ViewerSmsFragment_;

@EActivity(R.layout.pager_layout)
public class MessagesActivity extends BaseActivity<MessagesPresenter> {
    @ViewById(R.id.pager)
    ViewPager mPager;

    @Override
    protected MessagesPresenter createPresenter() {
        return new MessagesPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initPager() {
        pages.add(new CreatingSmsFragment_());
        pages.add(new ContactFinderFragment_());
        pages.add(new NumberFinderFragment_());
        pages.add(new ViewerSmsFragment_());
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }
}
