package pl.edu.agh.guideblind.messages.incoming.new_messages;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;

@EFragment(R.layout.page_with_view)
public class NewMessagesFragment extends BaseFragment<NewMessagesPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Override
    protected NewMessagesPresenter createPresenter() {
        return new NewMessagesPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        speak();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.new_messages));
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.sms_list));
    }
}