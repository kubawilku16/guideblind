package pl.edu.agh.guideblind.messages.page.viewer.conversations;

import android.support.v4.view.ViewPager;

import com.annimon.stream.Stream;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.messages.page.viewer.conversations.page.ConversationFragment;
import pl.edu.agh.guideblind.messages.page.viewer.conversations.page.ConversationFragment_;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;

@EActivity(R.layout.pager_layout)
public class ConversationsViewerActivity extends BaseActivity<ConversationsViewerPresenter> {
    @ViewById(R.id.pager)
    ViewPager mPager;

    @Override
    protected ConversationsViewerPresenter createPresenter() {
        return new ConversationsViewerPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initPager(List<ConversationRealm> conversations) {
        pages.addAll(Stream.of(conversations).map(conversation -> createFragment(conversation, conversations.indexOf(conversation))).toList());
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }

    private ConversationFragment createFragment(ConversationRealm conversation, int conversationIndex){
        ConversationFragment fragment  = ConversationFragment_.builder().conversation(conversation.getContactId()).build();
        if (conversationIndex == 0){
            fragment.setFirstFragment(true);
        }
        return fragment;
    }
}
