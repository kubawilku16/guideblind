package pl.edu.agh.guideblind.pictureRecognition.pictureRecognitonHome;

import pl.edu.agh.guideblind.base.BasePresenter;

class PictureRecognitionFragmentPresenter extends BasePresenter<PictureRecognitionFragment> {
    public void onInit() {
        view.setImage();
    }

    public void onButtonClick() {
        view.startTimer();
    }
}
