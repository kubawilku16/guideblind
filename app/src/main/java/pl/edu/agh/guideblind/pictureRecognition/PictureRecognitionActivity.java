package pl.edu.agh.guideblind.pictureRecognition;

import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.home.page.calling.CallingFragment_;
import pl.edu.agh.guideblind.home.page.informations.InformationFragment_;
import pl.edu.agh.guideblind.home.page.messages.MessagesFragment_;
import pl.edu.agh.guideblind.home.page.notes.NotesFragment_;
import pl.edu.agh.guideblind.pictureRecognition.caputrePicture.CapturePictureFragment_;
import pl.edu.agh.guideblind.pictureRecognition.pictureRecognitonHome.PictureRecognitionFragment_;

@EActivity(R.layout.pager_layout)
public class PictureRecognitionActivity extends BaseActivity<PictureRecognitionPresenter>{
    @ViewById(R.id.pager)
    ViewPager mPager;
    @Override
    protected PictureRecognitionPresenter createPresenter() {
        return new PictureRecognitionPresenter();
    }
    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initPages() {
        pages.add(new CapturePictureFragment_());
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }

}
