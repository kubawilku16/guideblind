package pl.edu.agh.guideblind.calling;

import pl.edu.agh.guideblind.base.BasePresenter;

class CallingPresenter extends BasePresenter<CallingActivity> {

    void onInit() {
        view.addPages();
        view.initAdapter();
    }
}