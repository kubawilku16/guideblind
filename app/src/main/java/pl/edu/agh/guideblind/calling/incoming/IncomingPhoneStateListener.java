package pl.edu.agh.guideblind.calling.incoming;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.widget.Toast;

import lombok.AllArgsConstructor;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

@AllArgsConstructor
class IncomingPhoneStateListener extends PhoneStateListener {
    private Context context;

    public void onCallStateChanged(int state, String incomingNumber) {
        if (state == 1) {
            Toast.makeText(context, incomingNumber, Toast.LENGTH_LONG).show();
            IncomingCallActivity_.intent(context).number(incomingNumber).flags(FLAG_ACTIVITY_NEW_TASK).start();
        }
    }
}