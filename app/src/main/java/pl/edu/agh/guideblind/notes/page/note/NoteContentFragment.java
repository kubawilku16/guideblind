package pl.edu.agh.guideblind.notes.page.note;

import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import lombok.Getter;
import lombok.Setter;
import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.notes.NotesViewerActivity_;

@EFragment(R.layout.note_page)
public class NoteContentFragment extends BaseFragment<NoteContentPresenter> {
    @ViewById(R.id.note_content)
    TextView contentView;
    @ViewById(R.id.note_date)
    TextView dateView;
    @FragmentArg
    String content;
    @FragmentArg
    Long date;

    @Setter
    @Getter
    private boolean clickToRemove = false;

    @Override
    protected NoteContentPresenter createPresenter() {
        return new NoteContentPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.note_layout)
    void onClick() {
        if (!clickToRemove) {
            presenter.onClick();
        } else {
            presenter.onRemoveClick();
        }
    }

    @LongClick(R.id.note_layout)
    boolean onLongClick() {
        presenter.stopSpeaker();
        presenter.onLongClick();
        return false;
    }

    @Override
    public void speak() {
        presenter.speak(new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault()).format(date) +
                ".\n" + content);
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                    }
                }
            }, 5000);
        } else {
            callButtonFirstClicked = true;
        }
    }

    void setContentAndDate() {
        contentView.setText(content);
        dateView.setText(new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault()).format(date));
    }

    void startRemoveTimer() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> clickToRemove = false);
                }
            }
        }, 5000);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> clickToRemove = true);
                }
            }
        }, 1000);
    }

    void startActivityAgain() {
        NotesViewerActivity_.intent(getActivity()).start();
        getActivity().finish();
    }
}