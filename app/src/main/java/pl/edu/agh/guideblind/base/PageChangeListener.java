package pl.edu.agh.guideblind.base;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import java.util.List;

public class PageChangeListener implements ViewPager.OnPageChangeListener {

    private ViewPager mPager;
    private List<Fragment> pages;

    private int mCurrentPosition;
    private int mScrollState;

    public PageChangeListener(ViewPager mPager, List<Fragment> pages) {
        this.mPager = mPager;
        this.pages = pages;
    }

    @Override
    public void onPageSelected(final int position) {
        FragmentLifecycle fragmentToShow = (FragmentLifecycle) pages.get(position);
        fragmentToShow.onResumeFragment();

        FragmentLifecycle fragmentToHide = (FragmentLifecycle) pages.get(mCurrentPosition);
        fragmentToHide.onPauseFragment();

        mCurrentPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(final int state) {
        mScrollState = state;
    }

    @Override
    public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
    }
}