package pl.edu.agh.guideblind.interactor;

import lombok.AllArgsConstructor;
import pl.edu.agh.guideblind.model.ConversationModel;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import pl.edu.agh.guideblind.utils.RxUtils;
import rx.Observable;

@AllArgsConstructor
public class GetConversationInteractor {
    private String contactId;

    public Observable<ConversationModel> execute() {
        return Observable.just(new ConversationModel(getContact(), getConversations())).compose(RxUtils.applySchedulers());
    }

    private ContactRealm getContact() {
        return RealmProvider.findById(ContactRealm.class, "id", contactId);
    }

    private ConversationRealm getConversations() {
        return RealmProvider.findById(ConversationRealm.class, "contactId", contactId);
    }
}