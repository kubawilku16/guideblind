package pl.edu.agh.guideblind.notes;

import android.util.Log;

import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetNotesInteractor;
import pl.edu.agh.guideblind.realm.entities.NoteRealm;

class NotesViewerPresenter extends BasePresenter<NotesViewerActivity> {

    void onInit() {
        new GetNotesInteractor().execute().subscribe(this::onNext, this::onError);
    }

    private void onNext(List<NoteRealm> noteRealms) {
        view.loadData(noteRealms);
    }

    private void onError(Throwable throwable) {
        Log.e(getClass().getName(), throwable.getMessage());
        speak(view.getResources().getString(R.string.unrecognized_error));
    }
}