package pl.edu.agh.guideblind.utils;

public class SpeakingUtils {
    private static String isAnyWordOnStringRegex = ".*[A-Za-z].*";
    private static String separatingStringToDetachedCharactersRegex = ".(?=.)";

    public static String getFormattedReceiverName(String name){
        if (!name.matches(isAnyWordOnStringRegex)){
            return name.replaceAll(separatingStringToDetachedCharactersRegex, "$0 ");
        }
        return name;
    }
}
