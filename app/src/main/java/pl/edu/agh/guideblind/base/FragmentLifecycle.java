package pl.edu.agh.guideblind.base;

public interface FragmentLifecycle extends BaseView {
    void onPauseFragment();
    void onResumeFragment();
}