package pl.edu.agh.guideblind.messages;

import pl.edu.agh.guideblind.base.BasePresenter;

class MessagesPresenter extends BasePresenter<MessagesActivity> {
    void onInit() {
        view.initPager();
        view.initAdapter();
    }
}