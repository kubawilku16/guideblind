package pl.edu.agh.guideblind.messages.conversation.page.content;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFirstFragment;
import pl.edu.agh.guideblind.messages.conversation.created.SmsSender;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.new_message_content)
public class NewMessageContentFragment extends BaseFirstFragment<NewMessageContentPresenter> {
    @ViewById(R.id.new_message_content_text)
    protected TextView textView;

    private static String content = "";

    @Override
    public NewMessageContentPresenter createPresenter() {
        return new NewMessageContentPresenter();
    }

    @Click(R.id.new_message_content_layout)
    public void onClick() {
        presenter.onCLick();
        wentToDownIntent = true;
    }

    @LongClick(R.id.new_message_content_layout)
    boolean onLongClick() {
        presenter.onLongClick();
        return false;
    }

    @Override
    public void speak() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(() -> presenter.speak(getResources().getString(R.string.tell_content)));
            }}
                , 500);
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            if (TextUtils.isEmpty(content)) {
                speak();
            } else {
                presenter.speak(content);
            }
            while (presenter.isSpeaking()) {
            }
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
        }
    }

    void startSpeechRecognizerIntent() {
        textView.setVisibility(View.GONE);
        SpeechRecognizer.startIntent(this);
    }

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            String extra = data.getStringArrayListExtra(EXTRA_RESULTS).get(0);
            textView.setText(extra);
            textView.setVisibility(View.VISIBLE);
            content = extra;
            if (getActivity() instanceof SmsSender) {
                ((SmsSender) getActivity()).saveSmsContent(content);
            }
        }
    }
}

