package pl.edu.agh.guideblind.interactor;

import android.content.Context;
import android.database.Cursor;
import android.provider.Telephony;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.RealmList;
import lombok.AllArgsConstructor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;
import pl.edu.agh.guideblind.realm.entities.MessageRealm;
import pl.edu.agh.guideblind.realm.entities.PhoneRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import pl.edu.agh.guideblind.utils.RxUtils;
import rx.Completable;

@AllArgsConstructor
public class SynchronizeSmsInteractor {
    private Context context;

    public Completable execute() {
        return Completable.fromCallable(this::extractConversations)
                .compose(RxUtils.applySchedulersCompletable());
    }

    private List<ConversationRealm> extractConversations() {
        return saveToRealm(context.getContentResolver().query(Telephony.Sms.CONTENT_URI, null, null, null, null));
    }

    private List<ConversationRealm> saveToRealm(Cursor cursor) {
        return cursor != null && cursor.getCount() > 0 ?
                RealmProvider.insertOrUpdate(getConversations(cursor)) : new ArrayList<>();
    }

    private List<ConversationRealm> getConversations(Cursor cursor) {
        List<ContactRealm> contacts = RealmProvider.findAll(ContactRealm.class);
        List<ConversationRealm> conversations = new ArrayList<>();
        while (cursor.moveToNext()) {
            String number = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.ADDRESS));
            String type = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.TYPE));
            final String content = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.BODY));
            Date date = new Date(Long.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.DATE))));
            try {
                Phonenumber.PhoneNumber phoneNumber = PhoneNumberUtil.getInstance().parse(number, Locale.getDefault().getCountry());
                number = String.valueOf(phoneNumber.getNationalNumber());
            } catch (NumberParseException e) {
                e.printStackTrace();
            }
            if (number == null){
                continue;
            }
            String finalNumber = number;
            Optional<ContactRealm> contact = Stream.of(contacts)
                    .filter(c -> findContact(finalNumber, c.getPhones()))
                    .findFirst();

            if (!contact.isPresent()) {
                RealmList<PhoneRealm> phones = new RealmList<>();
                phones.add(new PhoneRealm(number));
                contact = Optional.of(new ContactRealm(finalNumber, "unnamed", phones));
            }
            ContactRealm finalContact = contact.get();
            Optional<ConversationRealm> conversation = Stream.of(conversations)
                    .filter(c -> c.getContactId().equals(finalContact.getId()))
                    .findFirst();
            if (conversation.isPresent()) {
                conversations.remove(conversation.get());
                conversation.get().getMessages()
                        .add(new MessageRealm(content, date.getTime(), type));
                conversations.add(conversation.get());
            } else {
                conversations.add(createNewConversation(finalContact, content, date, type));
            }
        }
        return conversations;
    }

    private ConversationRealm createNewConversation(ContactRealm contact, String content, Date date, String type) {
        RealmList<MessageRealm> messages = new RealmList<>();
        messages.add(new MessageRealm(content, date.getTime(), type));
        return new ConversationRealm(contact.getId(), messages);
    }

    private boolean findContact(String number, RealmList<PhoneRealm> phones) {
        return Stream.of(phones)
                .filter(phone -> phone.getNumber().replaceAll(" ", "").contains(number))
                .findFirst().isPresent();
    }
}
