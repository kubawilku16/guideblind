package pl.edu.agh.guideblind.home.page.messages;

import pl.edu.agh.guideblind.base.BasePresenter;

class MessagesPresenter extends BasePresenter<MessagesFragment>{

    void onInit() {
        view.setImage();
    }

    void onButtonClick() {
        if(view != null) {
            view.startTimer();
        }
    }
}