package pl.edu.agh.guideblind.pojo.YandexResponsePojo;

import lombok.Getter;

@Getter
public class YandexResponse {
    private String code;
    private String lang;
    private String[] text;
}
