package pl.edu.agh.guideblind.calling.outgoingCall;

import pl.edu.agh.guideblind.base.BasePresenter;

public class OutgoingCallPresenter extends BasePresenter<OutgoingCallActivity> {

    void onInit() {
        view.setImage();
    }

    public void onClick() {
        view.finishConversation();
        view.goToCallActivity();
    }
}
