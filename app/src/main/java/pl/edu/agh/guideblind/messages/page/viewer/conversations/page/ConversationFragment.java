package pl.edu.agh.guideblind.messages.page.viewer.conversations.page;

import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import lombok.Getter;
import lombok.Setter;
import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.messages.conversation.founded.MessagesSlideActivity_;
import pl.edu.agh.guideblind.utils.SpeakingUtils;

@EFragment(R.layout.conversation_page)
public class ConversationFragment extends BaseFragment<ConversationPresenter> {
    @ViewById(R.id.conversation_page_name)
    TextView nameView;

    @Setter
    boolean firstFragment = false;

    @Getter
    @FragmentArg
    String conversation;

    @Override
    public ConversationPresenter createPresenter() {
        return new ConversationPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.conversation_page_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @Override
    public void speak() {
            String buttonText = nameView.getText().toString();
            presenter.speak(SpeakingUtils.getFormattedReceiverName(buttonText));
    }

    void setNameViewText(String name) {
        nameView.setText(name);
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    if (getActivity() != null){
                        getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                    }
                }
            }, 5000);
        } else {
            callButtonFirstClicked = true;
            wentToDownIntent = true;
            MessagesSlideActivity_.intent(getActivity()).contactId(conversation).start();
        }
    }

    void callOnBack() {
        getActivity().onBackPressed();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (firstFragment){
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    speak();
                }
            }, 500);
        }
    }
}
