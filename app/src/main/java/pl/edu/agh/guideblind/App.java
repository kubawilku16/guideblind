package pl.edu.agh.guideblind;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.speech.tts.TextToSpeech;
import android.text.TextUtils;

import org.androidannotations.annotations.EApplication;

import java.util.Locale;

import pl.edu.agh.guideblind.realm.provider.RealmProvider;

@EApplication
public class App extends Application {
    private static Context APPLICATION_CONTEXT;
    private TextToSpeech speech;

    @Override
    public void onCreate() {
        APPLICATION_CONTEXT = getApplicationContext();
        RealmProvider.init(APPLICATION_CONTEXT);
        if (speech == null) {
            speech = new TextToSpeech(APPLICATION_CONTEXT, this::onInit);
        }
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (speech != null) {
                    checkConnectivityStateAndSpeak(context, intent);
                }
            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        super.onCreate();
    }

    void checkConnectivityStateAndSpeak(Context context, Intent intent) {
        if (TextUtils.isEmpty(intent.getAction()) || intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            return;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return;
        }
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            speak(context.getResources().getString(R.string.wifi_disconnected));
            return;
        }
        if (isWifiConnected(networkInfo)) {
            speak(context.getResources().getString(R.string.wifi_connected) + networkInfo.getExtraInfo());
        } else if (isMobileDataConnected(networkInfo)) {
            speak(context.getResources().getString(R.string.mobile_connected));
        }
    }

    private void speak(String textToSpeech) {
        while (speech.isSpeaking()) {
            //ignored, to do not let 2 speakers speaks in same time
        }
        speech.speak(textToSpeech, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    private boolean isMobileDataConnected(NetworkInfo networkInfo) {
        return networkInfo.getType() == ConnectivityManager.TYPE_MOBILE && networkInfo.isConnected();
    }

    private boolean isWifiConnected(NetworkInfo networkInfo) {
        return networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected();
    }

    private void onInit(int status) {
        while (true) {
            if (TextToSpeech.SUCCESS == status) {
                this.speech.setLanguage(Locale.getDefault());
                break;
            }
        }
    }

    public static Context getAppContext() {
        return APPLICATION_CONTEXT;
    }
}
