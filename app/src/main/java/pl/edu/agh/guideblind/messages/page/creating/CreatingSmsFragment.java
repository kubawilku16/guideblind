package pl.edu.agh.guideblind.messages.page.creating;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFirstFragment;
import pl.edu.agh.guideblind.messages.conversation.created.NewMessageSlideActivity_;

@EFragment(R.layout.page_with_view)
public class CreatingSmsFragment extends BaseFirstFragment<CreatingSmsPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Override
    public CreatingSmsPresenter createPresenter() {
        return new CreatingSmsPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.create_message));
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.sms_add));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
            NewMessageSlideActivity_.intent(getActivity()).start();
        }
    }
}