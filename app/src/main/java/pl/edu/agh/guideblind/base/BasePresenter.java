package pl.edu.agh.guideblind.base;

public class BasePresenter<V extends BaseView> {
    protected V view;
    private Speaker speaker = new Speaker();

    public void createView(V view){
        this.view = view;
    }

    public void initSpeaker(){speaker.initSpeaker();}

    public void speak(String textToSpeech){
        speaker.speak(textToSpeech);
    }

    public void stopSpeaker() {
        speaker.stopSpeaker();
    }

    public void shutdownSpeaker() {
        speaker.shutdownSpeaker();
    }

    public boolean isSpeaking(){
        return speaker.isSpeaking();
    }
}