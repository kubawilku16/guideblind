package pl.edu.agh.guideblind.pojo.AzureVisionPojo;

import lombok.Getter;

@Getter
public class Description
{
    private String[] tags;

    private Captions[] captions;

    @Override
    public String toString()
    {
        return "ClassPojo [tags = "+tags+", captions = "+captions+"]";
    }
}
