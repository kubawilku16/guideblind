package pl.edu.agh.guideblind.contacts.contact;


public interface ContactAdder {

    void saveContact();
    void saveContactName(String contactNames);
    void saveContactNumber(String contactNumber);
    String getContactName();
    String getContactNumber();
}
