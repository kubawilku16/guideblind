package pl.edu.agh.guideblind.notes.page.empty;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;

@EFragment(R.layout.page_with_view)
public class NoNotesFragment extends BaseFragment<NoNotesPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Override
    protected NoNotesPresenter createPresenter() {
        return new NoNotesPresenter();
    }

    @AfterViews
    void onInit(){
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    void onClick(){
        presenter.onClick();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.no_notes_fragment));
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.no_content));
    }
}