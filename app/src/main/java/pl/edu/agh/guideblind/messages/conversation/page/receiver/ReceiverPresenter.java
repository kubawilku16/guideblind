package pl.edu.agh.guideblind.messages.conversation.page.receiver;

import android.text.TextUtils;
import android.util.Patterns;

import com.annimon.stream.Stream;

import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;

class ReceiverPresenter extends BasePresenter<ReceiverFragment> {

    void onClick() {
        if (view != null) {
            view.startTimer();
        }
    }

    void onLongClick() {
        if (view != null){
            view.startSpeechRecognizerIntent();
        }
    }

    void searchForNumber(String number) {
        if(!TextUtils.isEmpty(number) && Patterns.PHONE.matcher(number).matches()) {
            view.sendNumberToSender(number);
        } else {
            speak(view.getContext().getResources().getString(R.string.incorrect_receivers));
        }
    }

    void searchForContacts(String contacts) {
        List<ContactRealm> contactsList = RealmProvider.findAll(ContactRealm.class);
        List<ContactRealm> filteredList = Stream.of(contactsList).filter(contact -> contacts.contains(contact.getName())).toList();
        if(filteredList != null && !filteredList.isEmpty()) {
            view.sendReceiversToSender(Stream.of(filteredList).map(contact -> contact.getPhones().get(0).getNumber().replaceAll(" ", "")).toList());
        } else {
            speak(view.getContext().getResources().getString(R.string.incorrect_receivers));
        }
    }
}