package pl.edu.agh.guideblind.messages.conversation.page.receiver;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFirstFragment;
import pl.edu.agh.guideblind.messages.conversation.created.SmsSender;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;
import static android.view.View.GONE;

@EFragment(R.layout.receiver_layout)
public class ReceiverFragment extends BaseFirstFragment<ReceiverPresenter> {
    @ViewById(R.id.receiver_text)
    protected TextView textView;

    private String receivers;

    @Override
    public ReceiverPresenter createPresenter() {
        return new ReceiverPresenter();
    }

    @Click(R.id.receiver_layout)
    public void onClick() {
        presenter.onClick();
    }

    @LongClick(R.id.receiver_layout)
    boolean onLongClick() {
        presenter.onLongClick();
        return false;
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.tell_receivers));
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            if (TextUtils.isEmpty(receivers)) {
                speak();
            } else {
                presenter.speak(receivers);
            }
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = true;
        }
    }

    void startSpeechRecognizerIntent() {
        textView.setVisibility(GONE);
        SpeechRecognizer.startIntent(this);
    }

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            String input = data.getStringArrayListExtra(EXTRA_RESULTS).get(0);
            textView.setVisibility(View.VISIBLE);
            textView.setText(input);
            if (Character.isDigit(input.charAt(0))) {
                presenter.searchForNumber(input.replace(" ", ""));
            } else {
                presenter.searchForContacts(input);
            }
            receivers = input;
        }
    }

    void sendNumberToSender(String number) {
        if (getActivity() instanceof SmsSender) {
            ((SmsSender) getActivity()).saveReceivers(new String[]{number});
        }
    }

    void sendReceiversToSender(List<String> receivers) {
        if (getActivity() instanceof SmsSender) {
            ((SmsSender) getActivity()).saveReceivers(receivers.toArray(new String[receivers.size()]));
        }
    }
}

