package pl.edu.agh.guideblind.contacts.page.number;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.contacts.ContactActivity;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.find_by_number_page)
public class AddNumberFragment extends BaseFragment<AddNumberPresenter> {

    private String contactNumber;

    @ViewById(R.id.find_by_number_page_text)
    protected TextView textView;

    @Override
    public AddNumberPresenter createPresenter() {
        return new AddNumberPresenter();
    }

    @Click(R.id.find_by_number_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @LongClick(R.id.find_by_number_layout)
    boolean onLongButtonClick() {
        presenter.onLongButtonClick();
        return true;
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.tell_number));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = true;
        }
    }

    void startSpeechRecognizerIntent() {
        SpeechRecognizer.startIntent(this);
    }

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            contactNumber = data.getStringArrayListExtra(EXTRA_RESULTS).get(0);
            textView.setText(contactNumber);
            textView.setVisibility(View.VISIBLE);
            contactNumber = TextUtils.join("", contactNumber.split(" "));
            presenter.findContactByNumber(contactNumber);
        }
    }

    void setContactNumberContactActivity(){
        ((ContactActivity)this.getActivity()).saveContactNumber(contactNumber);
    }

    void hideTextView() {
        textView.setVisibility(View.INVISIBLE);
    }
}
