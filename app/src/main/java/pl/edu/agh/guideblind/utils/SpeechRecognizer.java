package pl.edu.agh.guideblind.utils;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.Locale;

import pl.edu.agh.guideblind.R;

public class SpeechRecognizer {
    public static final int REQ_CODE_SPEECH_INPUT = 100;

    public static void startIntent(Fragment fragment) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                fragment.getActivity().getResources().getString(R.string.speech_prompt));
        try {
            fragment.startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(fragment.getActivity().getApplicationContext(),
                    fragment.getActivity().getResources().getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }
}