package pl.edu.agh.guideblind.interactor;

import java.util.List;

import pl.edu.agh.guideblind.realm.entities.ConversationRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import pl.edu.agh.guideblind.utils.RxUtils;
import rx.Observable;

public class GetConversationsInteractor {
    public Observable<List<ConversationRealm>> execute() {
        return Observable.just(getFromRealm())
                .compose(RxUtils.applySchedulers());
    }

    private List<ConversationRealm> getFromRealm() {
        return RealmProvider.findAll(ConversationRealm.class);
    }
}