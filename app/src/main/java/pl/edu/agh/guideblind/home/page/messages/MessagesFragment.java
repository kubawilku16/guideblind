package pl.edu.agh.guideblind.home.page.messages;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.messages.MessagesActivity_;

@EFragment(R.layout.page_with_view)
public class MessagesFragment extends BaseFragment<MessagesPresenter> {
    @ViewById(R.id.page_with_view_image)
    protected ImageView image;

    @Override
    public MessagesPresenter createPresenter() {
        return new MessagesPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }


    @Click(R.id.page_with_view_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }


    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.messages));
    }


    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.sms_list));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
            wentToDownIntent = true;
            MessagesActivity_.intent(getActivity()).start();
        }
    }
}