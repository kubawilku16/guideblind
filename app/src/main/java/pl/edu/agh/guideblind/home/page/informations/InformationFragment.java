package pl.edu.agh.guideblind.home.page.informations;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;

@EFragment(R.layout.infromation_page)
public class InformationFragment extends BaseFragment<InformationPresenter> {
    @ViewById(R.id.date)
    public TextView date;
    @ViewById(R.id.clock)
    public TextClock clock;
    @ViewById(R.id.battery)
    public ImageView battery;

    private int batteryLevel = 100;

    @Override
    protected InformationPresenter createPresenter() {
        return new InformationPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initDateAndTime() {
        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        date.setText(formatter.format(currentDate));
    }

    @Click(R.id.info_page)
    public void onLayoutClick() {
        presenter.onLayoutClick();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.shutdownSpeaker();
    }


    @Override
    public void onPauseFragment() {
        presenter.stopSpeaker();
    }

    @Override
    public void onResumeFragment() {
        speak();
    }

    public void speak() {
        presenter.speak(new StringBuilder().append(getResources().getString(R.string.current_time))
                .append(clock.getText()).append(".\n")
                .append(date.getText()).append("\n")
                .append(getResources().getString(R.string.battery_level))
                .append(batteryLevel)
                .append("%")
                .toString());
    }

    void setBatteryLevel() {
        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (battery != null) {
                    int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                    int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                    boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                            status == BatteryManager.BATTERY_STATUS_FULL;
                    if (level == -1 || scale == -1) {
                        battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_50));
                    } else {
                        batteryLevel = (int) (((float) level / (float) scale) * 100.0f);
                        if (isCharging) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_charging));
                        } else if (batteryLevel > 98) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_100));
                        } else if (batteryLevel > 89) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_90));
                        } else if (batteryLevel > 79) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_80));
                        } else if (batteryLevel > 49) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_50));
                        } else if (batteryLevel > 29) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_30));
                        } else if (batteryLevel > 19) {
                            battery.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.battery_20));
                        }
                    }
                }
            }
        }, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }
}
