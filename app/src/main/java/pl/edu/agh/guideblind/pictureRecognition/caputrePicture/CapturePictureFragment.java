package pl.edu.agh.guideblind.pictureRecognition.caputrePicture;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFirstFragment;
import pl.edu.agh.guideblind.interactor.GetPhotoDescriptionInteractor;
import pl.edu.agh.guideblind.services.camera.PictureCapturingListener;
import pl.edu.agh.guideblind.services.camera.PictureCapturingService;
import pl.edu.agh.guideblind.services.camera.PictureCapturingServiceImpl;

@EFragment(R.layout.page_with_view)
public class CapturePictureFragment extends BaseFirstFragment<CapturePicturePresenter> implements PictureCapturingListener, ActivityCompat.OnRequestPermissionsResultCallback {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    private PictureCapturingService pictureCapturingService;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    @Override
    protected CapturePicturePresenter createPresenter() {
        return new CapturePicturePresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_image)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.capture_picture_for_recognition));
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.capture_picture));
    }

    void initCapturingService(){
        pictureCapturingService = PictureCapturingServiceImpl.getInstance(getActivity());
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
            wentToDownIntent = true;
            presenter.startRecognizing();
        }
    }
    public void capturePicture(){
        pictureCapturingService.startCapturing(this);
    }

    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        if (pictureData != null && pictureUrl != null){
            getActivity().runOnUiThread(() -> {
                if (pictureUrl.contains("0_pic.jpg")) {
                    showToast("FIRST PIC TAKEN");
                } else if (pictureUrl.contains("1_pic.jpg")) {
                    showToast("SECOND PIC TAKEN");
                }
            });
        }
    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
        if (picturesTaken != null && !picturesTaken.isEmpty()) {
            showToast("Done capturing all photos!");
            byte[] image = picturesTaken.get(picturesTaken.firstKey());
            sendRequestToAzureApi(image);
            return;
        }
        showToast("No camera detected!");
    }

    private void sendRequestToAzureApi(byte[] photo){
        new GetPhotoDescriptionInteractor(photo, getContext()).execute().subscribe(x -> onNext(x.getText()[0]), this::onError);
    }

    private void onNext(String text){
        presenter.speak(text);
    }

    private void onError(Throwable throwable){
        presenter.speak(getResources().getString(R.string.unrecognized_error) + throwable.getMessage());
    }

    private void showToast(final String text) {
        getActivity().runOnUiThread(() ->
                Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show()
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final String[] requiredPermissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            showToast("Some permissions missed");
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }
}
