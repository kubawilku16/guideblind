package pl.edu.agh.guideblind.contacts;


import pl.edu.agh.guideblind.base.BasePresenter;

class ContactPresenter extends BasePresenter<ContactActivity> {

    void onInit(){
        view.initPager();
        view.initAdapter();
    }
}

