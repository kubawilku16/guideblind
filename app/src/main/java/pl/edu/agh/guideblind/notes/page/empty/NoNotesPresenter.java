package pl.edu.agh.guideblind.notes.page.empty;

import pl.edu.agh.guideblind.base.BasePresenter;

class NoNotesPresenter extends BasePresenter<NoNotesFragment> {
    void onInit() {
        view.setImage();
    }

    void onClick() {
        if (view != null) {
            view.speak();
        }
    }
}