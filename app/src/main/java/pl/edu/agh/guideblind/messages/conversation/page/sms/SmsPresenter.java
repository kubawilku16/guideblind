package pl.edu.agh.guideblind.messages.conversation.page.sms;

import android.util.Log;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetContactByNumberInteractor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;

class SmsPresenter  extends BasePresenter<SmsFragment>{

    void onInit(String smsFrom) {
        view.initData();
        getContactById(smsFrom);
    }

    private void getContactById(String smsFrom) {
        new GetContactByNumberInteractor(smsFrom).execute().subscribe(this::onNext, this::onError);
    }

    private void onNext(ContactRealm contactRealm) {
        if(contactRealm != null) {
            view.setContactName(contactRealm.getName());
        }
    }

    private void onError(Throwable throwable) {
        speak(view.getResources().getString(R.string.unrecognized_error));
        Log.e(getClass().getName(), "onError: ", throwable);
    }

    void onScreenClick() {
        if(view != null) {
            view.startTimer();
        }
    }
}