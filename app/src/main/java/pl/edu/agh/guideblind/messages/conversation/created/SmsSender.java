package pl.edu.agh.guideblind.messages.conversation.created;

public interface SmsSender {
    void saveReceivers(String[] receivers);
    void saveSmsContent(String content);
    void sendSms();
    String getReceiversAndContent();
    String getReceivers();
    String getContent();
}