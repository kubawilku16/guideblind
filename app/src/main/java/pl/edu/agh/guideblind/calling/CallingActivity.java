package pl.edu.agh.guideblind.calling;

import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.calling.page.contact.ContactCallingFragment_;
import pl.edu.agh.guideblind.calling.page.number.NumberCallingFragment_;

@EActivity(R.layout.pager_layout)
public class CallingActivity extends BaseActivity<CallingPresenter> {
    @ViewById(R.id.pager)
    ViewPager mPager;

    @Override
    public CallingPresenter createPresenter() {
        return new CallingPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void addPages() {
        pages.add(new NumberCallingFragment_());
        pages.add(new ContactCallingFragment_());
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }
}