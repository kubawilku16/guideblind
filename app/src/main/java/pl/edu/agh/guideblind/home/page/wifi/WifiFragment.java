package pl.edu.agh.guideblind.home.page.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.wifi.WifiSpotsActivity_;

@EFragment(R.layout.page_with_view)
public class WifiFragment extends BaseFragment<WifiPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    private WifiManager wifiManager;

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Override
    protected WifiPresenter createPresenter() {
        return new WifiPresenter();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        presenter.onClick();
    }

    @LongClick(R.id.page_with_view_layout)
    void onLongClick() {
        presenter.onLongClick();
    }

    @Override
    public void speak() {
        presenter.speak(getTextToSpeech());
    }

    private String getTextToSpeech() {
        return getResources().getString(!wifiManager.isWifiEnabled() ? R.string.wifi_on : wifiManager.startScan() ? R.string.wifi_spot_start : R.string.wifi_off);
    }

    void initWifiManager() {
        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), wifiManager.isWifiEnabled() ? R.drawable.wifi_on : R.drawable.wifi_off));
    }

    void changeWifiStatus() {
        wifiManager.setWifiEnabled(!wifiManager.isWifiEnabled());
    }

    void initWifiStateReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                image.setImageDrawable(ContextCompat.getDrawable(getContext(), wifiManager.isWifiEnabled() ? R.drawable.wifi_on : R.drawable.wifi_off));
            }
        }, intentFilter);
    }

    boolean isWifiEnabled() {
        return wifiManager.isWifiEnabled();
    }

    void startSpotsActivity() {
        WifiSpotsActivity_.intent(getActivity()).start();
    }
}
