package pl.edu.agh.guideblind.wifi.pages.no_spots;

import pl.edu.agh.guideblind.base.BasePresenter;

class NoSpotsPresenter extends BasePresenter<NoSpotsFragment> {

    void onInit() {
        view.setImage();
    }

    void onClick() {
        if(view != null) {
            view.speak();
        }
    }
}