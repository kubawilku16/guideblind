package pl.edu.agh.guideblind.calling.outgoingCall;

import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Method;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.home.HomeActivity_;

@EActivity(R.layout.page_with_view)
public class OutgoingCallActivity extends BaseActivity<OutgoingCallPresenter> {
    @ViewById(R.id.page_with_view_image)
    protected ImageView imageView;

    @Override
    protected OutgoingCallPresenter createPresenter() {
        return new OutgoingCallPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    public void onClick() {
        presenter.onClick();
    }

    void finishConversation() {
        try {
            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";

            Class<?> telephonyClass = Class.forName(telephonyName);

            Method getService = // getDefaults[29];
                    Class.forName(serviceManagerName).getMethod("getService", String.class);
            Method tempInterfaceMethod = Class.forName(serviceManagerNativeName).getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            Object serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyClass.getClasses()[0].getMethod("asInterface", IBinder.class);
            Object telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyClass.getMethod("endCall").invoke(telephonyObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setImage() {
        imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cellphone_end_call));
    }

    void goToCallActivity() {
        HomeActivity_.intent(this).start();
    }
}
