package pl.edu.agh.guideblind.wifi;

import pl.edu.agh.guideblind.base.BasePresenter;

class WifiSpotsPresenter extends BasePresenter<WifiSpotsActivity> {

    void onInit() {
        view.initWifiManager();
        view.scanForSpots();
    }
}