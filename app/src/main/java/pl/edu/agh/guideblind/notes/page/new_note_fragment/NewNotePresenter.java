package pl.edu.agh.guideblind.notes.page.new_note_fragment;

import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.realm.entities.NoteRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;

class NewNotePresenter extends BasePresenter<NewNoteFragment> {

    void onInit() {
        view.setImage();
    }

    void onClick() {
        if(view != null) {
            view.speak();
        }
    }

    void onLongButtonLick() {
        if(view != null){
            view.startSpeechRecognizerIntent();
        }
    }

    void saveNote(String content) {
        RealmProvider.insertOrUpdate(new NoteRealm(System.currentTimeMillis(), content));
        view.startActivityAgain();
    }
}