package pl.edu.agh.guideblind.base;

import java.util.Timer;
import java.util.TimerTask;

public abstract class BaseFirstFragment<P extends BasePresenter> extends BaseFragment<P> {
    @Override
    public void onStart() {
        super.onStart();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(() -> speak());
            }
        }, 500);
    }
}
