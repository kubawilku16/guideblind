package pl.edu.agh.guideblind.messages.conversation.page.sms;

import android.text.TextUtils;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.base.FragmentLifecycle;
import pl.edu.agh.guideblind.utils.SpeakingUtils;

@EFragment(R.layout.sms_page)
public class SmsFragment extends BaseFragment<SmsPresenter> implements FragmentLifecycle {
    @ViewById(R.id.sms_from)
    TextView fromText;
    @ViewById(R.id.sms_content_message)
    TextView contentText;
    @ViewById(R.id.sms_when)
    TextView whenText;

    @FragmentArg
    String smsContent;
    @FragmentArg
    String smsFrom;
    @FragmentArg
    String smsType;
    @FragmentArg
    Long smsDate;

    private String contactName = "";

    @Override
    protected SmsPresenter createPresenter() {
        return new SmsPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit(smsFrom);
    }

    @Click(R.id.sms_page)
    public void onScreenClick() {
        presenter.onScreenClick();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.shutdownSpeaker();
    }

    @Override
    public void speak() {
        StringBuilder sb = new StringBuilder();
        sb.append(getResources().getString(R.string.sms_fragment_message_at));
        sb.append(new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(smsDate));
        sb.append("\n");
        if (smsType.equals("2")){
            sb.append(getResources().getString(R.string.sms_fragment_message_from_owner));
        }
        else {
            sb.append(getResources().getString(R.string.sms_fragment_message_from));
            sb.append(SpeakingUtils.getFormattedReceiverName(getFrom()));
        }
        sb.append(smsContent);
        sb.append(".");

        presenter.speak(sb.toString());
    }

    void initData() {
        fromText.setText(smsFrom);
        contentText.setText(smsContent);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        whenText.setText(formatter.format(smsDate));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = true;
        }
    }

    void setContactName(String name) {
        this.contactName = name;
    }

    private String getFrom() {
        return !TextUtils.isEmpty(contactName) ? contactName : smsFrom;
    }
}
