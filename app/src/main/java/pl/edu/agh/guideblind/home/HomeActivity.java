package pl.edu.agh.guideblind.home;

import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.home.page.calling.CallingFragment_;
import pl.edu.agh.guideblind.home.page.contact.ContactFragment;
import pl.edu.agh.guideblind.home.page.contact.ContactFragment_;
import pl.edu.agh.guideblind.home.page.informations.InformationFragment_;
import pl.edu.agh.guideblind.home.page.last_location.LocationFragment_;
import pl.edu.agh.guideblind.home.page.messages.MessagesFragment_;
import pl.edu.agh.guideblind.home.page.notes.NotesFragment_;
import pl.edu.agh.guideblind.pictureRecognition.pictureRecognitonHome.PictureRecognitionFragment_;
import pl.edu.agh.guideblind.home.page.wifi.WifiFragment_;

@EActivity(R.layout.pager_layout)
public class HomeActivity extends BaseActivity<HomePresenter> {
    @ViewById(R.id.pager)
    ViewPager mPager;

    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initPages() {
        pages.add(new InformationFragment_());
        pages.add(new CallingFragment_());
        pages.add(new MessagesFragment_());
        pages.add(new NotesFragment_());
        pages.add(new PictureRecognitionFragment_());
        pages.add(new WifiFragment_());
        pages.add(new LocationFragment_());
        pages.add(new ContactFragment_());
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }
}