package pl.edu.agh.guideblind.calling.incoming;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import lombok.Getter;
import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;

@EActivity(R.layout.page_with_view)
public class IncomingCallActivity extends BaseActivity<IncomingCallPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Getter
    @Extra
    String number;

    @Getter
    private boolean isCalling = true;

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        isCalling = false;
        presenter.onClick();
    }

    @Override
    protected IncomingCallPresenter createPresenter() {
        return new IncomingCallPresenter();
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cellphone));
    }
}