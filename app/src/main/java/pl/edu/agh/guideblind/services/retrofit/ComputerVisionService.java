package pl.edu.agh.guideblind.services.retrofit;

import okhttp3.RequestBody;
import pl.edu.agh.guideblind.pojo.AzureVisionPojo.ImageDescription;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface ComputerVisionService {
    @Multipart
    @Headers("Ocp-Apim-Subscription-Key: 8a947fa4b5d341e7bac167687f1d0b34")
    @POST("describe")
    Observable<ImageDescription> describeImage(@Part("Binary image data") RequestBody photo);
}
