package pl.edu.agh.guideblind.messages.conversation.page.content;

import pl.edu.agh.guideblind.base.BasePresenter;

class NewMessageContentPresenter extends BasePresenter<NewMessageContentFragment> {

    void onCLick() {
        if(view != null){
            view.startTimer();
        }
    }

    void onLongClick() {
        if(view != null) {
            view.startSpeechRecognizerIntent();
        }
    }
}