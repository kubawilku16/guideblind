package pl.edu.agh.guideblind.messages.page.viewer.conversations;

import android.util.Log;

import java.util.Collections;
import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetConversationsInteractor;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;

class ConversationsViewerPresenter extends BasePresenter<ConversationsViewerActivity> {

    void onInit() {
        new GetConversationsInteractor().execute()
                .subscribe(this::onNext, this::onError);
    }

    private void onNext(List<ConversationRealm> conversationRealms) {
        if(conversationRealms != null && !conversationRealms.isEmpty()) {
            Collections.sort(conversationRealms, this::conversationComparator);
            view.initPager(conversationRealms);
            view.initAdapter();
        }
    }

    private int conversationComparator(ConversationRealm firstConversation, ConversationRealm secondConversation){
        long firstConversationDate = firstConversation.getMessages().first().getDate();
        long secondConversationDate = secondConversation.getMessages().first().getDate();
        if ( firstConversationDate > secondConversationDate ){
            return -1;
        }
        else if ( firstConversationDate < secondConversationDate){
            return 1;
        }
        return 0;
    }

    private void onError(Throwable throwable) {
        speak(view.getResources().getString(R.string.unrecognized_error));
        Log.e(getClass().getName(), throwable.getMessage());
        view.onBackPressed();
    }
}