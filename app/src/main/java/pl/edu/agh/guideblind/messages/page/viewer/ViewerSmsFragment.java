package pl.edu.agh.guideblind.messages.page.viewer;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.messages.page.viewer.conversations.ConversationsViewerActivity_;

@EFragment(R.layout.page_with_view)
public class ViewerSmsFragment extends BaseFragment<ViewerSmsPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @Override
    public ViewerSmsPresenter createPresenter() {
        return new ViewerSmsPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.page_with_view_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }


    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.sms_list));
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.view_sms));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
            wentToDownIntent = true;
            ConversationsViewerActivity_.intent(getActivity()).start();
        }
    }
}
