package pl.edu.agh.guideblind.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.wifi.pages.no_spots.NoSpotsFragment_;
import pl.edu.agh.guideblind.wifi.pages.spot.WifiSpotFragment_;

@EActivity(R.layout.pager_layout)
public class WifiSpotsActivity extends BaseActivity<WifiSpotsPresenter> {
    @ViewById(R.id.pager)
    ViewPager pager;

    private WifiManager wifiManager;

    @Override
    protected WifiSpotsPresenter createPresenter() {
        return new WifiSpotsPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initWifiManager() {
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    void initPagerAdapter() {
        pager.addOnPageChangeListener(new PageChangeListener(pager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        pager.setAdapter(mPagerAdapter);
    }

    void initPages(List<ScanResult> spots) {
        if (!spots.isEmpty()) {
            for (ScanResult spot : spots) {
                pages.add(WifiSpotFragment_.builder()
                        .SSID(spot.SSID)
                        .capabilities(spot.capabilities)
                        .isProtected(getScanResultSecurity(spot))
                        .build());
            }
        } else {
            pages.add(new NoSpotsFragment_());
        }
        initPagerAdapter();
    }

    void scanForSpots() {
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                List<ScanResult> spots = wifiManager.getScanResults();
                initPages(spots);
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    public static boolean getScanResultSecurity(ScanResult scanResult) {
        final String[] securityModes = { "WEP", "WPA", "WPA2", "WPA-EAP", "IEEE8021X" };
        for(String mode : securityModes) {
            if(scanResult.capabilities.contains(mode))
                return true;
        }
        return false;
    }
}