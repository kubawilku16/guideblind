package pl.edu.agh.guideblind.messages.conversation.page.sending;

import pl.edu.agh.guideblind.base.BasePresenter;

class SenderPresenter extends BasePresenter<SenderFragment> {
    void onInit() {
        view.bindData();
    }

    void onButtonClick() {
        if (view != null) {
            view.startTimer();
        }
    }

    void onLongButtonClick() {
        if( view != null) {
            view.sendSms();
        }
    }
}