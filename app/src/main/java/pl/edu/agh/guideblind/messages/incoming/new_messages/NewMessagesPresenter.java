package pl.edu.agh.guideblind.messages.incoming.new_messages;

import pl.edu.agh.guideblind.base.BasePresenter;

class NewMessagesPresenter extends BasePresenter<NewMessagesFragment> {
    void onInit() {
        view.setImage();
    }
}