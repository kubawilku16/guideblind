package pl.edu.agh.guideblind.messages.conversation.created;

import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.annimon.stream.Stream;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.messages.conversation.page.content.NewMessageContentFragment_;
import pl.edu.agh.guideblind.messages.conversation.page.receiver.ReceiverFragment_;
import pl.edu.agh.guideblind.messages.conversation.page.sending.SenderFragment_;
import pl.edu.agh.guideblind.utils.SmsSendUtils;

@EActivity(R.layout.pager_layout)
public class NewMessageSlideActivity extends BaseActivity<NewMessageSlidePresenter> implements SmsSender {
    @ViewById(R.id.pager)
    ViewPager mPager;

    private String[] receivers;
    private String smsContent;

    @Override
    protected NewMessageSlidePresenter createPresenter() {
        return new NewMessageSlidePresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void initPager() {
        pages.add(new ReceiverFragment_());
        pages.add(new NewMessageContentFragment_());
        pages.add(new SenderFragment_());
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void saveReceivers(String[] receivers) {
        this.receivers = receivers;
    }

    @Override
    public void saveSmsContent(String content) {
        this.smsContent = content;
    }

    @Override
    public void sendSms() {
        if (receivers != null && receivers.length > 0 && !TextUtils.isEmpty(smsContent)) {
            if (SmsSendUtils.sendSms(smsContent, receivers)) {
                presenter.speak(getResources().getString(R.string.successful_send_sms));
                onBackPressed();
            } else {
                presenter.speak(getResources().getString(R.string.error_send_sms));
            }
        }
    }

    @Override
    public String getReceiversAndContent() {
        if (TextUtils.isEmpty(smsContent) && isReceiversEmpty()) {
            return getResources().getString(R.string.no_content_and_receivers);
        } else if (TextUtils.isEmpty(smsContent)) {
            return getResources().getString(R.string.no_message_content);
        } else if (isReceiversEmpty()) {
            return getResources().getString(R.string.no_message_receivers);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(getResources().getString(R.string.sms_content)).append(smsContent).append(".\n").append(getResources().getString(R.string.receivers)).append("\n");
            Stream.of(receivers).forEach(receiver -> sb.append(getResources().getString(R.string.number)).append(receiver).append(".\n"));
            return sb.toString();
        }
    }

    @Override
    public String getReceivers() {
        if (receivers == null || receivers.length == 0) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            for (String rec : receivers) {
                sb.append(rec).append(", ");
            }
            return sb.toString().substring(0, sb.toString().length() - 1);
        }
    }

    @Override
    public String getContent() {
        return smsContent;
    }

    private boolean isReceiversEmpty() {
        return receivers == null || receivers.length == 0 || !isAnyReceiver();
    }

    private boolean isAnyReceiver() {
        for (String rec : receivers) {
            if (!TextUtils.isEmpty(rec)) {
                return true;
            }
        }
        return false;
    }
}