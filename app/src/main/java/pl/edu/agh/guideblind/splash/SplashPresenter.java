package pl.edu.agh.guideblind.splash;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;

class SplashPresenter extends BasePresenter<SplashActivity> {
    void onInit() {
        initSpeaker();
        view.askForPermissions();
    }

    void checkPermissions(int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != 0) {
                speak(view.getResources().getString(R.string.not_all_accepted));
                view.finishApp();
                return;
            }
        }
        view.startHomeActivity();
    }
}