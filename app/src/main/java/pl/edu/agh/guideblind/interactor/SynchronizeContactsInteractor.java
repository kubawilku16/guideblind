package pl.edu.agh.guideblind.interactor;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;


import io.realm.RealmList;
import lombok.AllArgsConstructor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.entities.PhoneRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import pl.edu.agh.guideblind.utils.RxUtils;
import rx.Completable;
import rx.Observable;

@AllArgsConstructor
public class SynchronizeContactsInteractor {
    private static final String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
    private static final String PHONE_ID = ContactsContract.CommonDataKinds.Phone._ID;
    private static final String PHONE_NAME = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;

    private Context context;

    public Completable execute() {
        return Completable.fromCallable(this::extractContacts)
                .compose(RxUtils.applySchedulersCompletable());
    }

    private List<ContactRealm> extractContacts() {
        return Stream.of(context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{PHONE_ID, PHONE_NAME, PHONE_NUMBER}, null, null,
                PHONE_NAME + " ASC"))
                .map(this::saveToRealm)
                .single();
    }

    private List<ContactRealm> saveToRealm(Cursor cursor) {
        return RealmProvider.insertOrUpdate(getContacts(cursor));
    }

    private List<ContactRealm> getContacts(Cursor cursor) {
        List<ContactRealm> contacts = new ArrayList<>();
        if (cursor != null) {
            int phoneNameIndex = cursor.getColumnIndex(PHONE_NAME);
            int phoneNumberIndex = cursor.getColumnIndex(PHONE_NUMBER);
            int phoneIdIndex = cursor.getColumnIndex(PHONE_ID);
            while (cursor.moveToNext()) {
                RealmList<PhoneRealm> phones = new RealmList<>();
                phones.add(new PhoneRealm((cursor.getString(phoneNumberIndex))));
                contacts.add(new ContactRealm(cursor.getString(phoneIdIndex), cursor.getString(phoneNameIndex), phones));
            }
            cursor.close();
        }
        return contacts;
    }
}