package pl.edu.agh.guideblind.splash;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.annimon.stream.Stream;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CHANGE_NETWORK_STATE;
import static android.Manifest.permission.CHANGE_WIFI_STATE;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.SEND_SMS;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

class UtilPermissions {
    static String[] PERMISSIONS = {
            READ_CONTACTS,
            CALL_PHONE,
            RECORD_AUDIO,
            READ_SMS,
            SEND_SMS,
            READ_PHONE_STATE,
            RECEIVE_SMS,
            CHANGE_WIFI_STATE,
            ACCESS_WIFI_STATE,
            ACCESS_NETWORK_STATE,
            CHANGE_NETWORK_STATE,
            ACCESS_COARSE_LOCATION,
            ACCESS_FINE_LOCATION,
    };

    static boolean hasPermissions(Context context) {
        return !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null)
                || !Stream.of(PERMISSIONS).anyMatch(permission -> ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED);
    }
}
