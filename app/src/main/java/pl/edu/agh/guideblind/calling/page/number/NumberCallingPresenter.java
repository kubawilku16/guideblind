package pl.edu.agh.guideblind.calling.page.number;

import pl.edu.agh.guideblind.base.BasePresenter;

class NumberCallingPresenter extends BasePresenter<NumberCallingFragment> {

    void onButtonClick() {
        if(view != null) {
            view.speak();
        }
    }

    void onLongButtonLick() {
        if(view != null){
            view.hideTextView();
            view.startSpeechRecognizerIntent();
        }
    }

    void onCall(String number) {
        view.onCall(number);
    }
}