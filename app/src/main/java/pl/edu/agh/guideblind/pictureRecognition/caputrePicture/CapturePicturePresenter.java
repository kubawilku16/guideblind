package pl.edu.agh.guideblind.pictureRecognition.caputrePicture;

import pl.edu.agh.guideblind.base.BasePresenter;

public class CapturePicturePresenter extends BasePresenter<CapturePictureFragment> {

    public void onInit() {
        view.setImage();
        view.initCapturingService();
    }

    public void onButtonClick() {
        view.startTimer();
    }

    public void startRecognizing() {
        view.capturePicture();
    }
}
