package pl.edu.agh.guideblind.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements FragmentLifecycle {
    protected Boolean callButtonFirstClicked = false;
    protected boolean wentToDownIntent;
    protected P presenter;

    public BaseFragment() {
        super();
        if (presenter == null) {
            presenter = createPresenter();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.createView(this);
        presenter.initSpeaker();
    }

    protected abstract P createPresenter();

    public abstract void speak();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.shutdownSpeaker();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (wentToDownIntent){
            speak();
            wentToDownIntent = false;
        }
    }

    @Override
    public void onPauseFragment() {
        presenter.stopSpeaker();
        callButtonFirstClicked = false;
    }

    @Override
    public void onResumeFragment() {
        speak();
        callButtonFirstClicked = true;
    }
}