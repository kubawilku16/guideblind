package pl.edu.agh.guideblind.messages.page.contact;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.messages.conversation.founded.MessagesSlideActivity_;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.find_by_name_page)
public class ContactFinderFragment extends BaseFragment<ContactFinderPresenter> {

    @ViewById(R.id.find_by_name_page_text)
    protected TextView textView;

    @Override
    public ContactFinderPresenter createPresenter() {
        return new ContactFinderPresenter();
    }

    @Click(R.id.find_by_name_layout)
    public void onScreenClick() {
        presenter.onButtonClick();
    }

    @LongClick(R.id.find_by_name_layout)
    boolean onLongScreenClick() {
        presenter.onLongButtonClick();
        return true;
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.tell_contact));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
        }
    }

    void startSpeechRecognizerIntent() {
        SpeechRecognizer.startIntent(this);
    }

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            List<String> words = Arrays.asList(data.getStringArrayListExtra(EXTRA_RESULTS).get(0).split(" "));
            String contactName = words.size() < 2 ?
                    words.get(0) : words.get(0) + " " + words.get(1);
            contactName = WordUtils.capitalize(contactName);
            textView.setText(contactName);
            textView.setVisibility(View.VISIBLE);
            presenter.findContact(contactName);
        }
    }

    void startMessagesSlideActivity(ContactRealm contactRealm) {
        MessagesSlideActivity_.intent(getActivity()).contactId(contactRealm.getId()).start();
    }

    void hideTextView() {
        textView.setVisibility(View.INVISIBLE);
    }
}
