package pl.edu.agh.guideblind.messages.page.viewer;

import pl.edu.agh.guideblind.base.BasePresenter;

class ViewerSmsPresenter extends BasePresenter<ViewerSmsFragment> {
    void onInit() {
        view.setImage();
    }

    void onButtonClick() {
        if (view != null) {
            view.startTimer();
        }
    }
}