package pl.edu.agh.guideblind.interactor;


import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.List;

import lombok.AllArgsConstructor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;
import rx.Observable;

@AllArgsConstructor
public class GetContactByNumberInteractor {
    private String contactId;

    public Observable<ContactRealm> execute(){
        return Observable.defer(this::findContact);
    }

    private Observable<ContactRealm> findContact() {
        List<ContactRealm> contacts = RealmProvider.findAll(ContactRealm.class);
        Optional<ContactRealm> result = Stream.of(contacts)
                .filter(contact -> contact.getId().equals(contactId)).findFirst();
        return result.isPresent() ? Observable.just(result.get()) : Observable.just(null);
    }
}