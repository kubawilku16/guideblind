package pl.edu.agh.guideblind.contacts.page.name;


import android.util.Log;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetContactInteractor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;

public class AddContactFinderPresenter extends BasePresenter<AddContactFinderFragment> {

    void onButtonClick() {
        view.startTimer();
    }

    void onLongButtonClick() {
        if (view != null) {
            view.hideTextView();
            view.startSpeechRecognizerIntent();
        }
    }

    void findContact(String contactName) {
        new GetContactInteractor(contactName).execute()
                .subscribe(this::onNext, this::onError);
    }

    private void onNext(ContactRealm contactRealm) {
        if(contactRealm == null){
            view.setContactNameForActivity();
        }else {
            speak(view.getResources().getString(R.string.contact_exist));
        }
    }

    private void onError(Throwable throwable) {
        speak(view.getResources().getString(R.string.unrecognized_error));
        Log.e(getClass().getName(), throwable.getMessage());
    }
}
