package pl.edu.agh.guideblind.home.page.notes;

import pl.edu.agh.guideblind.base.BasePresenter;

class NotesPresenter extends BasePresenter<NotesFragment> {

    void onInit() {
        view.setImage();
    }

    void onClick() {
        if(view != null) {
            view.startTimer();
        }
    }
}