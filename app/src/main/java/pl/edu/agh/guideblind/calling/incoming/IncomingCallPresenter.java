package pl.edu.agh.guideblind.calling.incoming;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Method;

import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetContactByNumberInteractor;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;

class IncomingCallPresenter extends BasePresenter<IncomingCallActivity> {

    void onInit() {
        view.setImage();
        initSpeaker();
        findContact(view.getNumber());
    }

    private void findContact(String number) {
        new GetContactByNumberInteractor(number).execute()
                .subscribe(this::onSuccessGetContact, this::onErrorGetContact);
    }

    private void onSuccessGetContact(ContactRealm contactRealm) {
        if (contactRealm != null) {
            speak(contactRealm.getName() + "  is calling");
        } else {
            speak(view.getNumber() + "  is calling");
        }
    }

    private void onErrorGetContact(Throwable throwable) {
        Log.e("GET_CALLING_CONTACT", throwable.getMessage());
    }

    void onClick() {
        if (view != null) {
            try {
                stopSpeaker();
                TelephonyManager tm = (TelephonyManager) view.getApplicationContext()
                        .getSystemService(Context.TELEPHONY_SERVICE);
                Class<?> classTelephony = Class.forName(tm.getClass().getName());
                Method method = classTelephony.getDeclaredMethod("getITelephony");
                // Disable access check
                method.setAccessible(true);
                // Invoke getITelephony() to get the ITelephony interface
                Object telephonyInterface = method.invoke(tm);
                // Get the endCall method from ITelephony
                Class<?> telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());
                Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("answerRingingCall");
                // Invoke endCall()
                methodEndCall.invoke(telephonyInterface);

//                if (tm == null) {
//                    // this will be easier for debugging later on
//                    throw new NullPointerException("tm == null");
//                }
//                // do reflection magic
//                tm.getClass().getMethod("answerRingingCall").invoke(tm);
            } catch (Exception e) {
                // we catch it all as the following things could happen:
                // NoSuchMethodException, if the answerRingingCall() is missing
                // SecurityException, if the security manager is not happy
                // IllegalAccessException, if the method is not accessible
                // IllegalArgumentException, if the method expected other arguments
                // InvocationTargetException, if the method threw itself
                // NullPointerException, if something was a null value along the way
                // ExceptionInInitializerError, if initialization failed
                // something more crazy, if anything else breaks
                speak("Error while answering a call");
                // TODO decide how to handle this state
                // you probably want to set some failure state/go to fallback
                Log.e("ERROR_ANSWER_CALLING", e.getMessage());
            }

        }
    }
}