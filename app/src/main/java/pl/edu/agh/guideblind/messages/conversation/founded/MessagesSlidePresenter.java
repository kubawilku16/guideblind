package pl.edu.agh.guideblind.messages.conversation.founded;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.GetConversationInteractor;
import pl.edu.agh.guideblind.model.ConversationModel;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;

class MessagesSlidePresenter extends BasePresenter<MessagesSlideActivity> {
    void onInit(String contactId) {
        getConversation(contactId);
    }

    private void getConversation(String contactId) {
        new GetConversationInteractor(contactId).execute()
                .subscribe(this::onNext, this::onError);
    }

    private void onNext(ConversationModel model) {
        if (model.getConversation() != null) {
            view.initPager(model.getConversation(), model.getContact());
            view.initAdapter();
        } else {
            onError(new NullPointerException());
        }
    }

    private void onError(Throwable throwable) {
//        Log.e(getClass().getName(),throwable.getMessage());
        speak(view.getResources().getString(R.string.unrecognized_error));
        while (isSpeaking()) {
        }
        view.onBackPressed();
    }
}