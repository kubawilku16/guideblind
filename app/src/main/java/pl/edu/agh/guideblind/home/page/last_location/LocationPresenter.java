package pl.edu.agh.guideblind.home.page.last_location;

import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.Locale;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;

class LocationPresenter extends BasePresenter<LocationFragment>{

    void onInit() {
        view.initLocationClient();
    }

    void onClick() {
        if(view != null){
            view.speak();
        }
    }

    void onLongClick() {
        if(view != null){
            view.getLastKnownLocation();
        }
    }

    void onSuccessGetLocation(Location location) {
        if(location != null && view != null){
            try {
                Geocoder geocoder = new Geocoder(view.getActivity(), Locale.getDefault());
                view.setLocation(geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0).getAddressLine(0));
                view.speak();
            } catch (IOException e) {
                e.printStackTrace();
                view.setLocation("");
                onFailureGetLocation(new NullPointerException());
            }
        } else {
            onFailureGetLocation(new NullPointerException());
        }
    }

    void onFailureGetLocation(Throwable throwable){
        speak(view.getResources().getString(R.string.location_failure));
        Log.e(getClass().getSimpleName(), throwable.getMessage(), throwable);
    }
}