package pl.edu.agh.guideblind.messages.incoming;

import android.support.v4.view.ViewPager;

import com.annimon.stream.Stream;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.messages.conversation.page.sms.SmsFragment_;
import pl.edu.agh.guideblind.messages.incoming.new_messages.NewMessagesFragment_;

@EActivity(R.layout.pager_layout)
public class IncomingMessagesActivity extends BaseActivity<IncomingMessagesPresenter> {
    @ViewById(R.id.pager)
    ViewPager pager;

    @Extra
    String[] messages;
    @Extra
    String[] senders;
    @Extra
    Long[] dates;

    @Override
    protected IncomingMessagesPresenter createPresenter() {
        return new IncomingMessagesPresenter();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void addPages() {
        pages.add(new NewMessagesFragment_());
        Stream.range(0, messages.length).forEach(i -> pages.add(SmsFragment_.builder()
                .smsFrom(senders[i])
                .smsDate(dates[i])
                .smsContent(messages[i])
                .build()));
    }

    void initPager() {
        pager.addOnPageChangeListener(new PageChangeListener(pager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        pager.setAdapter(mPagerAdapter);
    }
}
