package pl.edu.agh.guideblind.calling.page.number;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFirstFragment;
import pl.edu.agh.guideblind.utils.SpeechRecognizer;

import static android.app.Activity.RESULT_OK;
import static android.speech.RecognizerIntent.EXTRA_RESULTS;

@EFragment(R.layout.find_by_number_page)
public class NumberCallingFragment extends BaseFirstFragment<NumberCallingPresenter> {

    @ViewById(R.id.find_by_number_page_text)
    protected TextView textView;

    @Override
    public NumberCallingPresenter createPresenter() {
        return new NumberCallingPresenter();
    }

    @Click(R.id.find_by_number_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @LongClick(R.id.find_by_number_layout)
    public synchronized boolean onLongButtonClick() {
        presenter.onLongButtonLick();
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.tell_number));
    }

    /**
     * Showing google speech input dialog
     */
    void startSpeechRecognizerIntent() {
        SpeechRecognizer.startIntent(this);
    }

    /**
     * Receiving speech input
     */

    @OnActivityResult(SpeechRecognizer.REQ_CODE_SPEECH_INPUT)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            String number = data.getStringArrayListExtra(EXTRA_RESULTS).get(0);
            textView.setText(number);
            textView.setVisibility(View.VISIBLE);
            presenter.onCall(TextUtils.join("", number.split(" ")));
        }
    }

    void onCall(String number) {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED && android.util.Patterns.PHONE.matcher(number).matches()) {
            getActivity().startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + number)));
        }
    }

    void hideTextView() {
        textView.setVisibility(View.INVISIBLE);
    }
}
