package pl.edu.agh.guideblind.contacts.page.save;

import pl.edu.agh.guideblind.base.BasePresenter;

class SaveContactPresenter extends BasePresenter<SaveContactFragment>{

    void onButtonClick() {
        if(view != null) {
            view.startTimer();
        }
    }

    void onLongButtonClick() {
        if(view != null) {
            view.hideTextView();
            view.saveContact();
        }
    }
}
