package pl.edu.agh.guideblind.home.page.notes;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.notes.NotesViewerActivity_;

@EFragment(R.layout.page_with_view)
public class NotesFragment extends BaseFragment<NotesPresenter> {
    @ViewById(R.id.page_with_view_image)
    ImageView image;

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Override
    protected NotesPresenter createPresenter() {
        return new NotesPresenter();
    }

    @Click(R.id.page_with_view_layout)
    void onClick() {
        presenter.onClick();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.notes));
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.note));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            NotesViewerActivity_.intent(getActivity()).start();
            callButtonFirstClicked = false;
            wentToDownIntent = true;
        }
    }
}