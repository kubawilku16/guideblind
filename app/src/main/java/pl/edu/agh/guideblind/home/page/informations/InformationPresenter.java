package pl.edu.agh.guideblind.home.page.informations;

import pl.edu.agh.guideblind.base.BasePresenter;

class InformationPresenter extends BasePresenter<InformationFragment> {
    void onInit() {
        view.setBatteryLevel();
        view.initDateAndTime();
    }

    void onLayoutClick() {
        if (view != null) {
            view.speak();
        }
    }
}