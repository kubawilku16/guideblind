package pl.edu.agh.guideblind.home.page.calling;

import pl.edu.agh.guideblind.base.BasePresenter;

class CallingPresenter extends BasePresenter<CallingFragment> {
    void onInit() {
        view.setImage();
    }

    void onButtonClick() {
        view.startTimer();
    }
}