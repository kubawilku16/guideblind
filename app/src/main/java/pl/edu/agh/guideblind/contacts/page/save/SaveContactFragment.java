package pl.edu.agh.guideblind.contacts.page.save;


import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.contacts.ContactActivity;


@EFragment(R.layout.save_contact_page)
public class SaveContactFragment extends BaseFragment<SaveContactPresenter> {
    @ViewById(R.id.save_contact_page_text)
    protected TextView textView;

    @Override
    public SaveContactPresenter createPresenter() {
        return new SaveContactPresenter();
    }

    @Click(R.id.save_contact_page_image)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @LongClick(R.id.save_contact_layout)
    boolean onLongButtonClick() {
        presenter.onLongButtonClick();
        return true;
    }
    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.save_contact));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            callButtonFirstClicked = true;
            speak();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = true;
        }
    }

    void hideTextView() {
        textView.setVisibility(View.INVISIBLE);
    }

    void saveContact(){
        ((ContactActivity)this.getActivity()).saveContact();
    }
}
