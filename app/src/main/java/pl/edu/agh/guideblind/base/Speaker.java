package pl.edu.agh.guideblind.base;

import android.speech.tts.TextToSpeech;

import java.util.Locale;

import pl.edu.agh.guideblind.App;

public class Speaker {
    private TextToSpeech speech;

    public void initSpeaker(){
        this.speech = new TextToSpeech(App.getAppContext(), this::onInit);
    }

    private void onInit(int status) {
        while(true){
            if (TextToSpeech.SUCCESS == status){
                this.speech.setLanguage(Locale.getDefault());
                break;
            }
        }
    }

    public boolean isSpeaking(){
        return speech.isSpeaking();
    }

    public void speak(String toSpeak) {
        this.speech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    void shutdownSpeaker() {
        if (this.speech != null) {
            this.speech.stop();
            this.speech.shutdown();
        }
    }

    void stopSpeaker() {
        if (this.speech != null) {
            this.speech.stop();
        }
    }
}