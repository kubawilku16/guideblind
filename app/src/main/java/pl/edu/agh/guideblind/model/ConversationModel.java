package pl.edu.agh.guideblind.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;

@Getter
@AllArgsConstructor
public class ConversationModel {
    private ContactRealm contact;
    private ConversationRealm conversation;
}