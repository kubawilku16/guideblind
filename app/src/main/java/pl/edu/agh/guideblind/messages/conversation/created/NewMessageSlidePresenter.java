package pl.edu.agh.guideblind.messages.conversation.created;

import pl.edu.agh.guideblind.base.BasePresenter;

class NewMessageSlidePresenter extends BasePresenter<NewMessageSlideActivity> {
    void onInit() {
        initSpeaker();
        view.initPager();
        view.initAdapter();
    }
}