package pl.edu.agh.guideblind.home.page.contact;


import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.contacts.ContactActivity_;

@EFragment(R.layout.page_with_view)
public class ContactFragment extends BaseFragment<ContactPresenter> {

    @ViewById(R.id.page_with_view_image)
    protected ImageView image;

    private boolean callButtonFirstClicked = false;

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Override
    protected ContactPresenter createPresenter() {
        return new ContactPresenter();
    }

    @Click(R.id.page_with_view_image)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @Override
    public void speak() {
        presenter.speak(getResources().getString(R.string.contacts));
    }

    void setImage() {
        image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.contact));
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
            wentToDownIntent = true;
            ContactActivity_.intent(getActivity()).start();
        }
    }
}
