package pl.edu.agh.guideblind.messages.page.creating;

import pl.edu.agh.guideblind.base.BasePresenter;

class CreatingSmsPresenter extends BasePresenter<CreatingSmsFragment> {

    void onInit() {
        view.setImage();
    }

    void onButtonClick() {
        if(view != null) {
            view.startTimer();
        }
    }
}