package pl.edu.agh.guideblind.notes;

import android.support.v4.view.ViewPager;

import com.annimon.stream.Stream;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.notes.page.empty.NoNotesFragment_;
import pl.edu.agh.guideblind.notes.page.new_note_fragment.NewNoteFragment_;
import pl.edu.agh.guideblind.notes.page.note.NoteContentFragment_;
import pl.edu.agh.guideblind.realm.entities.NoteRealm;

@EActivity(R.layout.pager_layout)
public class NotesViewerActivity extends BaseActivity<NotesViewerPresenter> {
    @ViewById(R.id.pager)
    ViewPager pager;

    @Override
    protected NotesViewerPresenter createPresenter() {
        return new NotesViewerPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    void loadData(List<NoteRealm> noteRealms) {
        pages.add(new NewNoteFragment_());
        if (noteRealms == null || noteRealms.isEmpty()) {
            pages.add(new NoNotesFragment_());
        } else {
            Stream.of(noteRealms).forEach(note -> pages.add(NoteContentFragment_
                    .builder()
                    .content(note.getContent())
                    .date(note.getDate())
                    .build()));
        }
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        pager.setAdapter(mPagerAdapter);
        pager.addOnPageChangeListener(new PageChangeListener(pager, pages));
    }
}