package pl.edu.agh.guideblind.messages.conversation.founded;

import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.annimon.stream.Stream;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseActivity;
import pl.edu.agh.guideblind.base.BasePagerAdapter;
import pl.edu.agh.guideblind.base.PageChangeListener;
import pl.edu.agh.guideblind.messages.conversation.created.SmsSender;
import pl.edu.agh.guideblind.messages.conversation.page.content.NewMessageContentFragment_;
import pl.edu.agh.guideblind.messages.conversation.page.sending.SenderFragment_;
import pl.edu.agh.guideblind.messages.conversation.page.sms.SmsFragment_;
import pl.edu.agh.guideblind.realm.entities.ContactRealm;
import pl.edu.agh.guideblind.realm.entities.ConversationRealm;
import pl.edu.agh.guideblind.utils.SmsSendUtils;

@EActivity(R.layout.pager_layout)
public class MessagesSlideActivity extends BaseActivity<MessagesSlidePresenter> implements SmsSender {
    @ViewById(R.id.pager)
    ViewPager mPager;

    @Extra
    String contactId;
    private String content;
    private String name;
    private String number;

    @Override
    protected MessagesSlidePresenter createPresenter() {
        return new MessagesSlidePresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit(contactId);
    }

    void initPager(ConversationRealm conversationRealm, ContactRealm contactRealm) {
        this.name = contactRealm.getName();
        this.number = contactRealm.getPhones().get(0).getNumber();
        pages.add(new NewMessageContentFragment_());
        pages.add(new SenderFragment_());
        Stream.of(conversationRealm.getMessages())
                .forEach(message -> pages.add(SmsFragment_.builder()
                        .smsFrom(name)
                        .smsDate(message.getDate())
                        .smsContent(message.getContent())
                        .smsType(message.getType())
                        .build()));
    }

    void initAdapter() {
        mPager.addOnPageChangeListener(new PageChangeListener(mPager, pages));
        mPagerAdapter = new BasePagerAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void saveReceivers(String[] receivers) {
        //Unnecessary, here is only one Receiver given as parameter
    }

    @Override
    public void saveSmsContent(String content) {
        this.content = content;
    }

    @Override
    public void sendSms() {
        if (content != null && !TextUtils.isEmpty(content)) {
            if (SmsSendUtils.sendSms(content, new String[]{number})) {
                presenter.speak(getResources().getString(R.string.successful_send_sms));
                onBackPressed();
            } else {
                presenter.speak(getResources().getString(R.string.error_send_sms));
            }
        }
    }

    @Override
    public String getReceiversAndContent() {
        if (TextUtils.isEmpty(content)) {
            return getResources().getString(R.string.no_message_content);
        } else {
            return getResources().getString(R.string.sms_content) +
                    content +
                    getResources().getString(R.string.receivers) + (TextUtils.isEmpty(name) ? contactId : name);
        }
    }

    @Override
    public String getReceivers() {
        return (TextUtils.isEmpty(name) ? contactId : name);
    }

    @Override
    public String getContent() {
        return content;
    }
}