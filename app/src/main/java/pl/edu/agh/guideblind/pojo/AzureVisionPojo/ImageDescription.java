package pl.edu.agh.guideblind.pojo.AzureVisionPojo;

import lombok.Getter;

@Getter
public class ImageDescription
{
    private String requestId;

    private Description description;

    private Metadata metadata;

    @Override
    public String toString()
    {
        return "ClassPojo [requestId = "+requestId+", description = "+description+", metadata = "+metadata+"]";
    }
}
