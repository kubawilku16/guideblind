package pl.edu.agh.guideblind.interactor;

import android.content.Context;

import java.util.Locale;

import lombok.AllArgsConstructor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.pojo.AzureVisionPojo.ImageDescription;
import pl.edu.agh.guideblind.pojo.YandexResponsePojo.YandexResponse;
import pl.edu.agh.guideblind.services.retrofit.ComputerVisionService;
import pl.edu.agh.guideblind.services.retrofit.RetrofitServiceFactory;
import pl.edu.agh.guideblind.utils.RxUtils;
import pl.edu.agh.guideblind.services.retrofit.YandexTranslationService;
import rx.Observable;

@AllArgsConstructor
public class GetPhotoDescriptionInteractor {
    private byte[] photo;
    private Context context;

    public Observable<YandexResponse> execute(){
        return getDescription();
    }

    private Observable<YandexResponse> getDescription(){
        ComputerVisionService visionService = RetrofitServiceFactory.createService(ComputerVisionService.class, context.getResources().getString(R.string.vision_api_endpoint));
        RequestBody rImage = RequestBody.create(MediaType.parse("image/*"), photo);
        Observable<ImageDescription> request = visionService.describeImage(rImage);
        return request.concatMap(x -> getTranslation(x.getDescription().getCaptions()[0].getText())).compose(RxUtils.applySchedulers());
    }

    private Observable<YandexResponse> getTranslation(String text){
        YandexTranslationService translationService = RetrofitServiceFactory.createService(YandexTranslationService.class, context.getResources().getString(R.string.yandex_base_url));
        String key = context.getResources().getString(R.string.yandex_key);
        return translationService.translate(key, Locale.getDefault().getLanguage(), text);
    }
}
