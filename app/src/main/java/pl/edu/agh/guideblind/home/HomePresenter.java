package pl.edu.agh.guideblind.home;

import android.util.Log;
import android.widget.Toast;

import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.interactor.SynchronizeContactsInteractor;
import pl.edu.agh.guideblind.interactor.SynchronizeSmsInteractor;

class HomePresenter extends BasePresenter<HomeActivity> {

    void onInit() {
        view.initPages();
        view.initAdapter();
        extractDataToRealm();
    }

    private void extractDataToRealm() {
        new SynchronizeContactsInteractor(view.getApplicationContext()).execute()
                .andThen(new SynchronizeSmsInteractor(view.getApplicationContext()).execute())
                .subscribe(this::onNext, this::onError);
    }

    private void onNext() {
        Toast.makeText(view.getApplicationContext(), "Successful extract data to realm", Toast.LENGTH_SHORT).show();
    }

    private void onError(Throwable throwable) {
        Log.e(getClass().getName(), throwable.getMessage());
    }
}