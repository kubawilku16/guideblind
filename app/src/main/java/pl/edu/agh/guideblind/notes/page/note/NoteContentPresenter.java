package pl.edu.agh.guideblind.notes.page.note;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BasePresenter;
import pl.edu.agh.guideblind.realm.entities.NoteRealm;
import pl.edu.agh.guideblind.realm.provider.RealmProvider;

class NoteContentPresenter extends BasePresenter<NoteContentFragment> {
    void onClick() {
        if (view != null) {
            view.startTimer();
        }
    }

    void onInit() {
        view.setContentAndDate();
    }

    void onLongClick() {
        if (view != null) {
            speak(view.getResources().getString(R.string.click_to_remove_note) + view.content);
            view.startRemoveTimer();
        }
    }

    void onRemoveClick() {
        if (view != null) {
            stopSpeaker();
            RealmProvider.delete(NoteRealm.class, "date", view.date);
            view.startActivityAgain();
        }
    }
}