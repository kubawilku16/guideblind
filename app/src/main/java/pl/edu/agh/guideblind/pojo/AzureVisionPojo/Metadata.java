package pl.edu.agh.guideblind.pojo.AzureVisionPojo;

import lombok.Getter;

@Getter
public class Metadata
{
    private String height;

    private String width;

    private String format;

    @Override
    public String toString()
    {
        return "ClassPojo [height = "+height+", width = "+width+", format = "+format+"]";
    }
}
