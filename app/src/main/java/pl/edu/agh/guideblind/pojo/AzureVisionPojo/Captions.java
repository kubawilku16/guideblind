package pl.edu.agh.guideblind.pojo.AzureVisionPojo;

import lombok.Getter;

@Getter
public class Captions
{
    private String text;

    private String confidence;

    @Override
    public String toString()
    {
        return "ClassPojo [text = "+text+", confidence = "+confidence+"]";
    }
}
