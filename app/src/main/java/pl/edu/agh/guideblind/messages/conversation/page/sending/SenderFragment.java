package pl.edu.agh.guideblind.messages.conversation.page.sending;

import android.text.TextUtils;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;
import pl.edu.agh.guideblind.messages.conversation.created.SmsSender;

@EFragment(R.layout.new_message_page)
public class SenderFragment extends BaseFragment<SenderPresenter> {
    @ViewById(R.id.new_message_receiver_text)
    protected TextView receiversText;
    @ViewById(R.id.new_message_text)
    protected TextView contentText;

    @Override
    protected SenderPresenter createPresenter() {
        return new SenderPresenter();
    }

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Click(R.id.new_message_layout)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    @LongClick(R.id.new_message_layout)
    boolean onLongButtonLick() {
        presenter.onLongButtonClick();
        return false;
    }

    @Override
    public void speak() {
        if (getActivity() instanceof SmsSender) {
            presenter.speak(((SmsSender) getActivity()).getReceiversAndContent());
        }
    }

    void bindData() {
        if (getActivity() instanceof SmsSender) {
            String receivers = ((SmsSender) getActivity()).getReceivers();
            startContentChangeWatching();
            if (TextUtils.isEmpty(receivers)) {
                startReceiversChangeWatching();
            } else {
                if (receiversText != null) {
                    receiversText.setText(receivers);
                }
            }
        }
    }

    private void startReceiversChangeWatching() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() instanceof SmsSender) {
                    String receivers = ((SmsSender) getActivity()).getReceivers();
                    if (TextUtils.isEmpty(receivers)) {
                        startReceiversChangeWatching();
                    } else {
                        if (receiversText != null) {
                            getActivity().runOnUiThread(() ->
                                    receiversText.setText(receivers));
                        }
                    }
                }
            }
        }, 1000);
    }

    private void startContentChangeWatching() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() instanceof SmsSender) {
                    String content = ((SmsSender) getActivity()).getContent();
                    if (!TextUtils.isEmpty(content) && contentText != null) {
                        getActivity().runOnUiThread(() ->
                                contentText.setText(content));
                    }
                    startContentChangeWatching();
                }
            }
        }, 1000);
    }

    void startTimer() {
        if (!callButtonFirstClicked) {
            speak();
            callButtonFirstClicked = true;
            while (presenter.isSpeaking()) {
                //Wait until speaker is speaking to not starting timer earlier
            }
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(() -> callButtonFirstClicked = false);
                }
            }, 5000);
        } else {
            callButtonFirstClicked = false;
        }
    }

    void sendSms() {
        if (getActivity() instanceof SmsSender) {
            ((SmsSender) getActivity()).sendSms();
            getActivity().onBackPressed();
        }
    }
}
