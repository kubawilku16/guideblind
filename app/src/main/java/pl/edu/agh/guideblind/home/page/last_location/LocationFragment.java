package pl.edu.agh.guideblind.home.page.last_location;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.edu.agh.guideblind.R;
import pl.edu.agh.guideblind.base.BaseFragment;

@EFragment(R.layout.location_page)
public class LocationFragment extends BaseFragment<LocationPresenter> {
    @ViewById(R.id.location_text)
    TextView locationText;

    private FusedLocationProviderClient locationClient;
    private String lastLocationAddress;
    private Long lastLocationTime;

    @AfterViews
    void onInit() {
        presenter.onInit();
    }

    @Override
    protected LocationPresenter createPresenter() {
        return new LocationPresenter();
    }

    @Override
    public void speak() {
        if (lastLocationTime != null && !TextUtils.isEmpty(lastLocationAddress)) {
            presenter.speak(getResources().getString(R.string.location_your_last)
                    + lastLocationAddress
                    + getResources().getString(R.string.location_update_time)
                    + new SimpleDateFormat("HH:mm d MMM", Locale.getDefault()).format(new Date(lastLocationTime))
                    + getResources().getString(R.string.location_hold)
            );
        } else {
            presenter.speak(getResources().getString(R.string.location_hold));
        }
    }

    @Click(R.id.location_layout)
    void onClick() {
        presenter.onClick();
    }

    @LongClick(R.id.location_layout)
    void onLongClick() {
        presenter.onLongClick();
    }

    void initLocationClient() {
        locationClient = LocationServices.getFusedLocationProviderClient(getContext());
    }

    @SuppressLint("MissingPermission")
    void getLastKnownLocation() {
        locationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), presenter::onSuccessGetLocation)
                .addOnFailureListener(getActivity(), presenter::onFailureGetLocation);
    }

    void setLocation(String address) {
        locationText.setText(address);
        lastLocationAddress = address;
        lastLocationTime = System.currentTimeMillis();
    }
}
